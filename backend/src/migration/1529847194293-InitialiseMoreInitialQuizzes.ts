import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialiseMoreInitialQuizzes1529847194293 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await insertQuiz(queryRunner, {
            title: 'Donald Trump or Michael Scott?',
            description: 'Can you tell the US President from the eccentric boss from the US TV series "The Office"?',
            challenges: [
                {
                    title: '"Make friends first, make sales second, make love third. In no particular order."',
                    solution: 'Michael Scott',
                    otherOption: 'Donald Trump',
                },
                {
                    title: '"Would I rather be feared or loved? Easy, both. I want people to be afraid of how much they love me."',
                    solution: 'Michael Scott',
                    otherOption: 'Donald Trump',
                },
                {
                    title: '"I give them money. I give them food. Not directly, but through the money. I heal them."',
                    solution: 'Michael Scott',
                    otherOption: 'Donald Trump',
                },
                {
                    title: '"You know what they say: Fool me once, strike one, but fool me twice... Strike three."',
                    solution: 'Michael Scott',
                    otherOption: 'Donald Trump',
                },
                {
                    title: '"White-collar? Blue-collar? I am collar-blind."',
                    solution: 'Michael Scott',
                    otherOption: 'Donald Trump',
                },
                {
                    title: '"I think I am actually humble. I think I’m much more humble than you would understand."',
                    solution: 'Donald Trump',
                    otherOption: 'Michael Scott',
                },
                {
                    title: '"I promise not to talk about your massive plastic surgeries that didn’t work."',
                    solution: 'Donald Trump',
                    otherOption: 'Michael Scott',
                },
                {
                    title: '"Just kiss. I don’t even wait."',
                    solution: 'Donald Trump',
                    otherOption: 'Michael Scott',
                },
                {
                    title: '"Actually, throughout my life, my two greatest assets have been mental stability and being, like, really smart."',
                    solution: 'Donald Trump',
                    otherOption: 'Michael Scott',
                },
                {
                    title: '"I called myself the king of debt. I’m the king of debt. I’m great with debt, nobody knows debt better than me."',
                    solution: 'Donald Trump',
                    otherOption: 'Michael Scott',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Scooter lyrics or not?',
            description: 'Scooter is a German dance group mainly popular in the 90s. Their lyrics can be... Interesting. Can you tell which lines are real and which are not?',
            challenges: [
                {
                    title: 'I want you back, so clean up the dish',
                    solution: 'Scooter lyrics',
                    otherOption: 'Not Scooter lyrics',
                },
                {
                    title: 'Respect to the man in the ice cream van',
                    solution: 'Scooter lyrics',
                    otherOption: 'Not Scooter lyrics',
                },
                {
                    title: 'Love, peace and unity. Siberia, the place to be.',
                    solution: 'Scooter lyrics',
                    otherOption: 'Not Scooter lyrics',
                },
                {
                    title: 'Elastic to plastic my platinum hit',
                    solution: 'Scooter lyrics',
                    otherOption: 'Not Scooter lyrics',
                },
                {
                    title: 'Coming down on the floor like a maniac',
                    solution: 'Scooter lyrics',
                    otherOption: 'Not Scooter lyrics',
                },
                {
                    title: 'I am the belly dancer, the necromancer',
                    solution: 'Not Scooter lyrics',
                    otherOption: 'Scooter lyrics',
                },
                {
                    title: 'Melt the stage like a microwave',
                    solution: 'Not Scooter lyrics',
                    otherOption: 'Scooter lyrics',
                },
                {
                    title: 'Conquer the dancefloor like Ghengis Khan',
                    solution: 'Not Scooter lyrics',
                    otherOption: 'Scooter lyrics',
                },
                {
                    title: 'Pumped up with a full cup',
                    solution: 'Not Scooter lyrics',
                    otherOption: 'Scooter lyrics',
                },
                {
                    title: 'See how they run like pigs from a gun',
                    solution: 'Not Scooter lyrics',
                    otherOption: 'Scooter lyrics',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Guess the Dutch idioms',
            description: 'Can you tell which are literally translated Dutch idioms, and which are not?',
            challenges: [
                {
                    title: 'Let them smell poop',
                    solution: 'Dutch idiom',
                    otherOption: 'Not a Dutch idiom',
                },
                {
                    title: 'Give them a cookie of their own dough',
                    solution: 'Dutch idiom',
                    otherOption: 'Not a Dutch idiom',
                },
                {
                    title: 'Have it under the thumb',
                    solution: 'Dutch idiom',
                    otherOption: 'Not a Dutch idiom',
                },
                {
                    title: 'Showing the back of your teeth',
                    solution: 'Dutch idiom',
                    otherOption: 'Not a Dutch idiom',
                },
                {
                    title: 'Although a monkey might wear a golden ring, it will remain an ugly thing',
                    solution: 'Dutch idiom',
                    otherOption: 'Not a Dutch idiom',
                },
                {
                    title: 'He who doesn’t have a dog hunts with a cat',
                    solution: 'Not a Dutch idiom',
                    otherOption: 'Dutch idiom',
                },
                {
                    title: 'Jumping from the cock to the donkey',
                    solution: 'Not a Dutch idiom',
                    otherOption: 'Dutch idiom',
                },
                {
                    title: 'Swallowing grass snakes',
                    solution: 'Not a Dutch idiom',
                    otherOption: 'Dutch idiom',
                },
                {
                    title: 'It fell between chairs',
                    solution: 'Not a Dutch idiom',
                    otherOption: 'Dutch idiom',
                },
                {
                    title: 'They slid in on a shrimp sandwich',
                    solution: 'Not a Dutch idiom',
                    otherOption: 'Dutch idiom',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Kanye West or Mahatma Gandhi?',
            description: 'We all know that Kanye West is one of the greatest thinkers of our time. Is he indistinguishable from Mahatma Gandhi, though? Take the quiz and find out!',
            challenges: [
                {
                    title: '"I don’t believe in the concept of an enemy."',
                    solution: 'Kanye West',
                    otherOption: 'Mahatma Gandhi',
                },
                {
                    title: '"Some people have to work within the existing consciousness while some people can shift the consciousness."',
                    solution: 'Kanye West',
                    otherOption: 'Mahatma Gandhi',
                },
                {
                    title: '"Make decisions based on love not fear."',
                    solution: 'Kanye West',
                    otherOption: 'Mahatma Gandhi',
                },
                {
                    title: '"Be here now. Be in the moment. The now is the greatest moment of our lives and it just keeps getting better."',
                    solution: 'Kanye West',
                    otherOption: 'Mahatma Gandhi',
                },
                {
                    title: '"If you want to see the true character of a person, watch the way they treat someone who can’t do anything for them."',
                    solution: 'Kanye West',
                    otherOption: 'Mahatma Gandhi',
                },
                {
                    title: '"Truth alone will endure, all the rest will be swept away before the tide of time."',
                    solution: 'Mahatma Gandhi',
                    otherOption: 'Kanye West',
                },
                {
                    title: '"The weak can never forgive. Forgiveness is the attribute of the strong."',
                    solution: 'Mahatma Gandhi',
                    otherOption: 'Kanye West',
                },
                {
                    title: '"My life is my message."',
                    solution: 'Mahatma Gandhi',
                    otherOption: 'Kanye West',
                },
                {
                    title: '"Always believe in your dreams, because if you don’t, you’ll still have hope."',
                    solution: 'Mahatma Gandhi',
                    otherOption: 'Kanye West',
                },
                {
                    title: '"Selfishness is blind."',
                    solution: 'Mahatma Gandhi',
                    otherOption: 'Kanye West',
                },
            ],
        });
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

interface InitialQuiz {
    title: string;
    description: string;
    challenges: Array<{
        title: string;
        solution: string;
        otherOption: string;
    }>;
}

async function insertQuiz(queryRunner: QueryRunner, quiz: InitialQuiz) {
    const [ insertedQuiz ]: [ { id: number } ] = await queryRunner.query(
        `INSERT INTO quiz ("title", "description", "version") VALUES ('${quiz.title}', '${quiz.description}', 0) RETURNING "id"`,
    );

    await Promise.all(quiz.challenges.map(async (challenge) => {
        const [ insertedChallenge ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${insertedQuiz.id}, '${challenge.title}', 0) RETURNING "id"`,
        );
        const [ insertedSolution ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${insertedChallenge.id}, '${challenge.solution}', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${insertedChallenge.id}, '${challenge.otherOption}', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${insertedSolution.id} WHERE "id"=${insertedChallenge.id}`,
        );
    }));
}
