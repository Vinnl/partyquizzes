import { RequestHandler } from 'express';
import { getConnection, OrderByCondition } from 'typeorm';
import * as RSS from 'rss';

import { GetQuizzesResponse } from '../../../frontend/src/lib/endpoints/quiz';
import { Quiz } from '../entity/quiz';

export const quizzesHandler: RequestHandler = async (req, res) => {
  const options: Partial<Options> = {};

  if (req.query.limit && typeof req.query.limit === 'string') {
    options.limit = parseInt(req.query.limit, 10);
  }
  if (req.query.exclude && typeof req.query.exclude === 'string') {
    options.exclude = (req.query.exclude as string).split(',').map((id) => parseInt(id, 10));
  }
  if (req.query.order && ['new', 'top', 'random'].indexOf(req.query.order) !== -1) {
    options.orderBy = req.query.order;
  }

  const quizzes = await getQuizzes(options);

  if (quizzes === null) {
    return res.status(404).send('Quizzes not found');
  }

  res.json(quizzes);

  return;
};

export const quizFeedHandler: RequestHandler = async (req, res) => {
  const quizzes = await getQuizzes({ orderBy: 'new' });

  if (quizzes === null) {
    return res.status(404).send('Quizzes not found');
  }

  const feed = new RSS({
    image_url: process.env.PUBLIC_URL + '/favicon.png',
    feed_url: process.env.PUBLIC_URL + '/feed/quizzes/new',
    site_url: process.env.PUBLIC_URL!,
    title: 'Newest Party Quizzes',
  });

  quizzes.forEach(quiz => {
    feed.item({
      date: quiz.createdAt,
      description: quiz.description || quiz.title,
      title: quiz.title,
      url: process.env.PUBLIC_URL + `/quiz/${quiz.id}`,
    });
  });

  res.setHeader('Content-Type', 'application/xml');
  return res.send(feed.xml());
};

interface Options {
  orderBy: 'new' | 'top' | 'random';
  limit: number;
  exclude: number[];
}

const defaultOptions: Options = {
  orderBy: 'top',
  limit: 10,
  exclude: [],
};

export const getQuizzes: (options?: Partial<Options>) => Promise<GetQuizzesResponse | null>
  = async (options = defaultOptions) => {
  options = {
    ...defaultOptions,
    ...options,
  };

  const connection = getConnection();
  const repository = connection.getRepository(Quiz);

  const order: OrderByCondition = {};
  switch(options.orderBy) {
    case 'random':
      // This is Postgres-specific:
      order['RANDOM()'] = 'ASC';
      break;
    case 'new':
      order['quiz.createdAt'] = 'DESC';
      break;
    case 'top':
      order['COUNT(result.id)'] = 'DESC'
      order['quiz.updatedAt'] = 'DESC';
      break;
  }

  let query = repository.createQueryBuilder('quiz')
    .leftJoin('quiz.results', 'result')
    .groupBy('quiz.id')
    .orderBy(order)
    .limit(options.limit);

  if (Array.isArray(options.exclude) && options.exclude.length > 0) {
    query = query.where('quiz.id NOT IN (:...ids)', { ids: options.exclude });
  }

  const quizzes = await query.getMany();

    if (!quizzes) {
      return null;
    }

    const results: GetQuizzesResponse = quizzes.map(quiz => ({
      createdAt: quiz.createdAt.toISOString(),
      id: quiz.id,
      description: quiz.description,
      title: quiz.title,
      updatedAt: quiz.updatedAt.toISOString(),
    }));

    return results;
}
