import { CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from 'typeorm';

import { Option } from './option';
import { QuizResult } from './quizResult';

@Entity('quizAnswer')
export class QuizAnswer {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => QuizResult, quizResult => quizResult.answers, { onDelete: 'CASCADE' })
  public quizResult: QuizResult;

  @ManyToOne(() => Option, option => option.answers, { onDelete: 'CASCADE' })
  public option: Option;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @VersionColumn()
  public version: number;
}
