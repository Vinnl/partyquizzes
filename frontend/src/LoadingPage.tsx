import * as React from 'react';

// tslint:disable-next-line:no-empty-interface
interface Props {}

export const LoadingPage: React.SFC<Props> = (props) => (
  <section className="section">
    <div className="container">
      {props.children}
    </div>
  </section>
);
