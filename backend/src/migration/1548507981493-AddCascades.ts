import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCascades1548507981493 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "quizResult" DROP CONSTRAINT "FK_c811d99f1d8343e768416861dd6"`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" DROP CONSTRAINT "FK_a2707a40ea9394eeefb8ab412a8"`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" DROP CONSTRAINT "FK_8650ac862940ef6098202fe103d"`);
        await queryRunner.query(`ALTER TABLE "option" DROP CONSTRAINT "FK_19413e09578f0fbfb311a178150"`);
        await queryRunner.query(`ALTER TABLE "challenge" DROP CONSTRAINT "FK_86968ca225cb39e5bcd760db7f6"`);
        await queryRunner.query(`ALTER TABLE "quizResult" ADD CONSTRAINT "FK_c811d99f1d8343e768416861dd6" FOREIGN KEY ("quizId") REFERENCES "quiz"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" ADD CONSTRAINT "FK_a2707a40ea9394eeefb8ab412a8" FOREIGN KEY ("quizResultId") REFERENCES "quizResult"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" ADD CONSTRAINT "FK_8650ac862940ef6098202fe103d" FOREIGN KEY ("optionId") REFERENCES "option"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "option" ADD CONSTRAINT "FK_19413e09578f0fbfb311a178150" FOREIGN KEY ("challengeId") REFERENCES "challenge"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "challenge" ADD CONSTRAINT "FK_86968ca225cb39e5bcd760db7f6" FOREIGN KEY ("quizId") REFERENCES "quiz"("id") ON DELETE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "challenge" DROP CONSTRAINT "FK_86968ca225cb39e5bcd760db7f6"`);
        await queryRunner.query(`ALTER TABLE "option" DROP CONSTRAINT "FK_19413e09578f0fbfb311a178150"`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" DROP CONSTRAINT "FK_8650ac862940ef6098202fe103d"`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" DROP CONSTRAINT "FK_a2707a40ea9394eeefb8ab412a8"`);
        await queryRunner.query(`ALTER TABLE "quizResult" DROP CONSTRAINT "FK_c811d99f1d8343e768416861dd6"`);
        await queryRunner.query(`ALTER TABLE "challenge" ADD CONSTRAINT "FK_86968ca225cb39e5bcd760db7f6" FOREIGN KEY ("quizId") REFERENCES "quiz"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "option" ADD CONSTRAINT "FK_19413e09578f0fbfb311a178150" FOREIGN KEY ("challengeId") REFERENCES "challenge"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" ADD CONSTRAINT "FK_8650ac862940ef6098202fe103d" FOREIGN KEY ("optionId") REFERENCES "option"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" ADD CONSTRAINT "FK_a2707a40ea9394eeefb8ab412a8" FOREIGN KEY ("quizResultId") REFERENCES "quizResult"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "quizResult" ADD CONSTRAINT "FK_c811d99f1d8343e768416861dd6" FOREIGN KEY ("quizId") REFERENCES "quiz"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
