import { InitialisedOption, InitialisedQuiz} from "../../../frontend/src/lib/endpoints/quiz";
import { Quiz as QuizRow } from "../entity/quiz";

export const rowToResponse: (row: QuizRow) => InitialisedQuiz = (quiz) => {
  return {
    challenges:
      quiz.challenges
      .map((challenge) => ({
        challenge: challenge.title,
        options: challenge.options.map(option => ({ title: option.title, id: option.id })) as [InitialisedOption, InitialisedOption],
        solution: challenge.options.findIndex(option => option.id === challenge.solution.id),
      })),
    id: quiz.id,
    title: quiz.title,
    description: quiz.description,
  };
}
