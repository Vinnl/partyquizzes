import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateQuizResult1529485964890 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "quizResult" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "quizId" integer, CONSTRAINT "PK_0d75168b67de7fa2c6806cbce14" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "quizAnswer" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "quizResultId" integer, "optionId" integer, CONSTRAINT "PK_cb43c97ac8526b53edf18b651f5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "quizResult" ADD CONSTRAINT "FK_c811d99f1d8343e768416861dd6" FOREIGN KEY ("quizId") REFERENCES "quiz"("id")`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" ADD CONSTRAINT "FK_a2707a40ea9394eeefb8ab412a8" FOREIGN KEY ("quizResultId") REFERENCES "quizResult"("id")`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" ADD CONSTRAINT "FK_8650ac862940ef6098202fe103d" FOREIGN KEY ("optionId") REFERENCES "option"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "quizAnswer" DROP CONSTRAINT "FK_8650ac862940ef6098202fe103d"`);
        await queryRunner.query(`ALTER TABLE "quizAnswer" DROP CONSTRAINT "FK_a2707a40ea9394eeefb8ab412a8"`);
        await queryRunner.query(`ALTER TABLE "quizResult" DROP CONSTRAINT "FK_c811d99f1d8343e768416861dd6"`);
        await queryRunner.query(`DROP TABLE "quizAnswer"`);
        await queryRunner.query(`DROP TABLE "quizResult"`);
    }

}
