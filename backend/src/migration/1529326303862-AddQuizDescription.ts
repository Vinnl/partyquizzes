import {MigrationInterface, QueryRunner} from "typeorm";

export class AddQuizDescription1529326303862 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "quiz" ADD "description" text`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "quiz" DROP COLUMN "description"`);
    }

}
