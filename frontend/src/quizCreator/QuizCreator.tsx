import * as React from 'react';
import * as ReactGA from 'react-ga';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

import './QuizCreator.css';

import { isValidQuiz, Option, PostQuizRequest, PostQuizResponse, Quiz } from '../lib/endpoints/quiz';
import { Sharer } from '../sharer/Sharer';
import { Challenge, ChallengeCreator } from './ChallengeCreator';

// tslint:disable-next-line:no-empty-interface
interface Props {}

interface State {
  readonly challenges: Challenge[];
  readonly title?: string;
  readonly description?: string;
  readonly id?: number;
}

export class QuizCreator extends React.Component<Props, State> {
  public readonly state: State = {
    challenges: [],
  };

  public constructor(props: Props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onCompleteChallenge = this.onCompleteChallenge.bind(this);
  }

  public render() {
    if (typeof this.state.id !== 'undefined') {
      return (
        <section className="section">
          <div className="container">
            <h2 className="title">Success!</h2>
            <p className="content">Your quiz <b>{this.state.title}</b> has been created.</p>
            <div className="content">
              <div className="field">
                <div className="control">
                  <Link
                    to={`/quiz/${this.state.id}`}
                    title={this.state.title}
                    className="button is-large is-primary"
                  >
                    Start quiz
                  </Link>
                </div>
              </div>
            </div>
            <div className="content">
              <Sharer
                url={`${document.location.protocol}//${document.location.host}/quiz/${this.state.id}`}
                message={`What will you score on my new quiz: ${this.state.title}`}
              />
            </div>
          </div>
        </section>
      );
    }

    return (
      <section className="section">
        <div className="container">
          <Helmet>
            <title>Quiz Maker · {process.env.REACT_APP_NAME}</title>
            <meta name="description" content="Create your own Party Quizzes with our Quiz Maker"/>
          </Helmet>
          <form
            onSubmit={this.onSubmit}
            id="quizCreatorForm"
          >
            <div className="field">
              <label htmlFor="quizTitle" className="label">
                Quiz title
              </label>
              <input
                id="quizTitle"
                onChange={this.onChangeTitle}
                required={true}
                type="text"
                className="input is-large"
                placeholder="Donald Trump or Michael Scott?"
              />
            </div>
            <div className="field">
              <label htmlFor="quizDescription" className="label">
                Description
              </label>
              <textarea
                onChange={this.onChangeDescription}
                className="textarea is-medium"
                placeholder="Can you tell the US President from the eccentric boss from the US TV series 'The Office'?"
              />
            </div>
          </form>
          <hr/>
          <ol>
            {this.renderCompletedChallenges()}
            <li className="media">
              <div className="media-content">
                <ChallengeCreator
                  key={`challenge${this.state.challenges.length}`}
                  id={this.state.challenges.length}
                  onComplete={this.onCompleteChallenge}
                  suggestedChallenge={this.state.challenges[this.state.challenges.length - 1]}
                />
              </div>
            </li>
          </ol>
          <div className="field">
            <div className="control">
              <button
                form="quizCreatorForm"
                disabled={!isValidQuiz(this.getProposedQuiz())}
                type="submit"
                className="button is-primary is-large"
              >
                Create quiz
              </button>
            </div>
          </div>
        </div>
      </section>
    );
  }

  private renderCompletedChallenges() {
    return this.state.challenges.map((challenge, index) => (
      <li className="media" key={`challenge${index}`}>
        <div className="media-content">
          <ChallengeCreator id={index} onComplete={this.onCompleteChallenge} challenge={challenge}/>
        </div>
      </li>
    ));
  }

  private async onSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const proposedQuiz: Partial<Quiz> = this.getProposedQuiz();

    if (!isValidQuiz(proposedQuiz)) {
      // TODO: Show error or prevent from happening by disabling submission button?
      return;
    }

    const request: PostQuizRequest = proposedQuiz;

    const headers = new Headers({
      'Content-Type': 'application/json',
    });

    ReactGA.event({
      action: 'Submit',
      category: 'QuizCreator',
    });

    const response = await fetch('/api/quizzes', {
      body: JSON.stringify(request),
      headers,
      method: 'POST',
    });

    const { id }: PostQuizResponse = await response.json();

    this.setState({ id });
  }

  private onChangeTitle(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ title: event.target.value });
  }

  private onChangeDescription(event: React.ChangeEvent<HTMLTextAreaElement>) {
    this.setState({ description: event.target.value });
  }

  private onCompleteChallenge(challengeIndex: number, challenge: Challenge) {
    this.setState(prevState => {
      const challenges = [ ...prevState.challenges ];
      challenges[challengeIndex] = challenge;

      return { challenges };
    });

    ReactGA.event({
      action: 'AddChallenge',
      category: 'QuizCreator',
    });
  }

  private getProposedQuiz(): Partial<Quiz> {
    return {
      challenges: this.state.challenges.map((challenge) => ({
        challenge: challenge.title,
        options: [ { title: challenge.solution }, { title: challenge.otherOptions[0]} ] as [Option, Option],
        solution: 0,
      })),
      description: this.state.description,
      title: this.state.title,
    };
  }
}
