export type QuizScores = Record<number, { score: number; total: number; }>;
export type ReferredByTwitter = boolean;
export type RefferredByFacebook = boolean;
export type RefferredByReddit = boolean;
