import * as React from 'react';
import { Link } from 'react-router-dom';

import { DataFetcherProps } from '../DataFetcher';
import { GetQuizzesResponse } from '../lib/endpoints/quiz';

type Props = DataFetcherProps<GetQuizzesResponse>;

export const QuizSuggestion: React.SFC<Props> = (props) => {
  if (!props.data || props.data.length === 0) {
    return null;
  }

  return (
    <p className="content">
      Next up:&nbsp;
      <Link
        to={`/quiz/${props.data[0].id}`}
        title={`Take the quiz: ${props.data[0].title}`}
      >
        {props.data[0].title}
      </Link>
    </p>
  );
};
