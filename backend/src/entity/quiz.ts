import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Challenge } from './challenge';
import { QuizResult } from './quizResult';

@Entity('quiz')
export class Quiz {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column('text')
  public title: string;

  @Column('text', { nullable: true })
  public description: string;

  @OneToMany(type => Challenge, challenge => challenge.quiz)
  public challenges: Challenge[];

  @OneToMany(type => QuizResult, result => result.quiz)
  public results: QuizResult[];

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @VersionColumn()
  public version: number;
}
