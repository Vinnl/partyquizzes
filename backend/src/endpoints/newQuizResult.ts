import { RequestHandler } from 'express';
import { getConnection, getRepository } from 'typeorm';

import { PostQuizResultRequest } from '../../../frontend/src/lib/endpoints/quiz';
import { Option } from '../entity/option';
import { Quiz } from '../entity/quiz';
import { QuizAnswer } from '../entity/quizAnswers';
import { QuizResult } from '../entity/quizResult';

export const newQuizResultHandler: RequestHandler = async (req, res) => {
  const submittedResult: Partial<PostQuizResultRequest> = req.body;
  if (typeof submittedResult !== 'object') {
    return;
  }

  const submittedAnswers = submittedResult.answers;
  if (
    typeof submittedResult.quizId !== 'number'
    || typeof submittedAnswers !== 'object'
    || !Array.isArray(submittedAnswers)
    || submittedAnswers.length <= 0
  ) {
    res.status(400).send('Not a valid quiz result');

    return;
  }

  const connection = getConnection();
  const quizRepository = getRepository(Quiz);
  const optionRepository = getRepository(Option);
  const quizResultRepository = getRepository(QuizResult);
  const quizAnswerRepository = getRepository(QuizAnswer);

  await connection.transaction(async (transactionEntityManager) => {
    const quiz = await quizRepository.findOne(submittedResult.quizId);

    const quizResult = quizResultRepository.create({
      quiz,
    });

    await transactionEntityManager.save(quizResult);

    const chosenOptions = await optionRepository.findByIds(submittedAnswers);

    const quizAnswers = quizAnswerRepository.create(chosenOptions.map((chosenOption) => ({
      option: chosenOption,
      quizResult,
    })));

    return await transactionEntityManager.save(quizAnswers);
  });

  res.status(200).send();

  return;
};
