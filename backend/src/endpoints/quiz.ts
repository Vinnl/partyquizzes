import { Request, RequestHandler } from 'express';
import { getConnection } from 'typeorm';

import { GetQuizResponse } from '../../../frontend/src/lib/endpoints/quiz';
import { Quiz } from '../entity/quiz';
import { rowToResponse } from '../lib/quizRowToResponse';

export const quizHandler: RequestHandler = async (req: Request & { params: { quizId: string } }, res) => {
  const quiz = await getQuiz(req.params.quizId);

  if (quiz === null) {
    return res.status(404).send('Quiz not found');
  }

  res.json(quiz);

  return;
};

export const getQuiz: (quizId: number) => Promise<GetQuizResponse | null> = async (quizId) => {
  const connection = getConnection();
  const repository = connection.getRepository(Quiz);

  const quiz = await repository.createQueryBuilder('quiz')
    .innerJoinAndSelect('quiz.challenges', 'challenge')
    .innerJoinAndSelect('challenge.options', 'option')
    .innerJoinAndSelect('challenge.solution', 'solution')
    .where({ id: quizId })
    .getOne();

  if (!quiz) {
    return null;
  }

  const results: GetQuizResponse = rowToResponse(quiz);

  return results;
};
