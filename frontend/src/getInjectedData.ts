import { InjectedData } from "./lib/endpoints/injectedData";

export const getInjectedData: () => InjectedData = () => {
  type DocumentWithData = Document & { __INJECTED_DATA?: InjectedData };
  const injectedData: InjectedData = (document && typeof (document as DocumentWithData).__INJECTED_DATA !== 'undefined')
    // Presumably due to the typecast TypeScript doesn't see that we can be sure that __INJECTED_DATA is not undefined,
    // hence the appended `!`
    ? (document as DocumentWithData).__INJECTED_DATA!
    : {};

  return injectedData;
}
