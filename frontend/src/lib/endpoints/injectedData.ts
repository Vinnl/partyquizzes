import { GetQuizzesResponse, InitialisedQuiz } from "./quiz";

export type Referrers = 'facebook' | 'reddit' | 'twitter';

export interface InjectedData {
  '/api/quizzes'?: GetQuizzesResponse;
  referrer?: Referrers;
  [ key: string ]: undefined | Referrers | GetQuizzesResponse | InitialisedQuiz;
}
