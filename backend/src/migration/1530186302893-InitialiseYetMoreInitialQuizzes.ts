import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialiseYetMoreInitialQuizzes1530186302893 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `UPDATE quiz SET "description"='Sometimes it just is hard to tell one from the other.' WHERE "id" IN (SELECT "id" FROM quiz WHERE title='George W. Bush or Captain America?' ORDER BY "createdAt" ASC LIMIT 1)`,
        );

        await insertQuiz(queryRunner, {
            title: 'Scientology or actual science?',
            description: 'Scientology, according to its founder L. Ron Hubbard, is the "science of knowledge". Let’s find out if you can distinguish Scientology’s beliefs about the world from actual science.',
            challenges: [
                {
                    title: 'A human is an immortal alien spiritual being (a "thetan"), that is presently trapped on planet Earth in a physical "meat body."',
                    solution: 'Scientology',
                    otherOption: 'Actual science',
                },
                {
                    title: '75 million years ago, aliens from a series of overpopulated planets were frozen by their Lord Xenu, then transported to earth to be dumped into Hawaii volcanoes.',
                    solution: 'Scientology',
                    otherOption: 'Actual science',
                },
                {
                    title: 'All world religions were implanted by Lord Xenu in our spiritual beings by making them watch a huge 3D movie for 36 days.',
                    solution: 'Scientology',
                    otherOption: 'Actual science',
                },
                {
                    title: 'It costs more than US$100,000 to learn about human origins.',
                    solution: 'Scientology',
                    otherOption: 'Actual science',
                },
                {
                    title: 'Modern aircraft is modelled after prehistoric spacecraft, but with jet engines.',
                    solution: 'Scientology',
                    otherOption: 'Actual science',
                },
                {
                    title: 'If you inject a weakened form of a disease into a person’s blood, that might make that person immune to the disease.',
                    solution: 'Actual science',
                    otherOption: 'Scientology',
                },
                {
                    title: 'Humans and monkeys share the same ancestors.',
                    solution: 'Actual science',
                    otherOption: 'Scientology',
                },
                {
                    title: 'The universe came into being after a Big Bang and has been expanding ever since.',
                    solution: 'Actual science',
                    otherOption: 'Scientology',
                },
                {
                    title: 'We can deliver a baby by cutting open a pregnant woman’s belly, taking out the baby, then sewing her back together.',
                    solution: 'Actual science',
                    otherOption: 'Scientology',
                },
                {
                    title: 'We’re living on a giant ball of lava floating through space.',
                    solution: 'Actual science',
                    otherOption: 'Scientology',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Actual startup or Startup generator',
            description: 'It sometimes feels like there’s a startup for everything. We pitted a few startup ideas from a startup idea generator against actual startups. Can you tell real from fake?',
            challenges: [
                {
                    title: 'Image recognition for bread',
                    solution: 'Actual startup',
                    otherOption: 'Startup generator',
                },
                {
                    title: 'Tinder for flying',
                    solution: 'Actual startup',
                    otherOption: 'Startup generator',
                },
                {
                    title: 'Tablets for dogs',
                    solution: 'Actual startup',
                    otherOption: 'Startup generator',
                },
                {
                    title: 'Twitter for credit cards',
                    solution: 'Actual startup',
                    otherOption: 'Startup generator',
                },
                {
                    title: 'Uber for water fountains',
                    solution: 'Actual startup',
                    otherOption: 'Startup generator',
                },
                {
                    title: 'Viral marketing for personal finances',
                    solution: 'Startup generator',
                    otherOption: 'Actual startup',
                },
                {
                    title: 'Flickr for marketers',
                    solution: 'Startup generator',
                    otherOption: 'Actual startup',
                },
                {
                    title: 'Snapchat for the homeless',
                    solution: 'Startup generator',
                    otherOption: 'Actual startup',
                },
                {
                    title: 'Fitbit for medicinal marijuana',
                    solution: 'Startup generator',
                    otherOption: 'Actual startup',
                },
                {
                    title: 'Crowdsourcing for funeral homes',
                    solution: 'Startup generator',
                    otherOption: 'Actual startup',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Linus Torvalds or Edsger Dijkstra',
            description: 'Two well-known people in the software industry with outspoken opinions. Can you tell which is whose?',
            challenges: [
                {
                    title: 'Do you pine for the days when men were men and wrote their own device drivers?',
                    solution: 'Linus Torvalds',
                    otherOption: 'Edsger Dijkstra',
                },
                {
                    title: 'Talk is cheap. Show me the code.',
                    solution: 'Linus Torvalds',
                    otherOption: 'Edsger Dijkstra',
                },
                {
                    title: 'It’s what I call "mental masturbation", when you engage is some pointless intellectual exercise that has no possible meaning.',
                    solution: 'Linus Torvalds',
                    otherOption: 'Edsger Dijkstra',
                },
                {
                    title: 'I’m always right. This time I’m just even more right than usual.',
                    solution: 'Linus Torvalds',
                    otherOption: 'Edsger Dijkstra',
                },
                {
                    title: 'Which mindset is right? Mine, of course. People who disagree with me are by definition crazy. (Until I change my mind, when they can suddenly become upstanding citizens. I’m flexible, and not black-and-white.)',
                    solution: 'Linus Torvalds',
                    otherOption: 'Edsger Dijkstra',
                },
                {
                    title: 'Several people have told me that my inability to suffer fools gladly is one of my main weaknesses.',
                    solution: 'Edsger Dijkstra',
                    otherOption: 'Linus Torvalds',
                },
                {
                    title: 'We must be very careful when we give advice to younger people: sometimes they follow it!',
                    solution: 'Edsger Dijkstra',
                    otherOption: 'Linus Torvalds',
                },
                {
                    title: 'Thank goodness we don’t have only serious problems, but ridiculous ones as well.',
                    solution: 'Edsger Dijkstra',
                    otherOption: 'Linus Torvalds',
                },
                {
                    title: 'Don’t blame me for the fact that competent programming (...) will be too difficult for "the average programmer".',
                    solution: 'Edsger Dijkstra',
                    otherOption: 'Linus Torvalds',
                },
                {
                    title: 'If you want more effective programmers, you will discover that they should not waste their time debugging, they should not introduce the bugs to start with.',
                    solution: 'Edsger Dijkstra',
                    otherOption: 'Linus Torvalds',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Is that a real programming language?',
            description: 'The creators of programming languages are an odd bunch, so sometimes, they create the weirdest programming languages. Can you tell which are real and which are not?',
            challenges: [
                {
                    title: 'D',
                    solution: 'Programming language',
                    otherOption: 'Not a programming language',
                },
                {
                    title: 'Brainfuck',
                    solution: 'Programming language',
                    otherOption: 'Not a programming language',
                },
                {
                    title: 'Whitespace',
                    solution: 'Programming language',
                    otherOption: 'Not a programming language',
                },
                {
                    title: 'Lolcode',
                    solution: 'Programming language',
                    otherOption: 'Not a programming language',
                },
                {
                    title: 'Shakespeare',
                    solution: 'Programming language',
                    otherOption: 'Not a programming language',
                },
                {
                    title: 'Ninja',
                    solution: 'Not a programming language',
                    otherOption: 'Programming language',
                },
                {
                    title: ';',
                    solution: 'Not a programming language',
                    otherOption: 'Programming language',
                },
                {
                    title: 'AAAAAAAAA!',
                    solution: 'Not a programming language',
                    otherOption: 'Programming language',
                },
                {
                    title: 'Supercalifragilisticexpialidocious',
                    solution: 'Not a programming language',
                    otherOption: 'Programming language',
                },
                {
                    title: 'Walmart',
                    solution: 'Not a programming language',
                    otherOption: 'Programming language',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Are you lucky?',
            description: 'The answers were selected randomly; are you lucky enough to guess the correct one?',
            challenges: [
                {
                    title: 'Which is the answer?',
                    solution: 'B',
                    otherOption: 'A',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'B',
                    otherOption: 'A',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
                {
                    title: 'Which is the answer?',
                    solution: 'A',
                    otherOption: 'B',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: '90s or 00s hits?',
            description: 'Which songs were hits in the 90s, and which in the 00s?',
            challenges: [
                {
                    title: 'U2 - Beautiful Day',
                    solution: '00s',
                    otherOption: '90s',
                },
                {
                    title: 'Pink - Get the Party Started',
                    solution: '00s',
                    otherOption: '90s',
                },
                {
                    title: 'Britney Spears - Toxic',
                    solution: '00s',
                    otherOption: '90s',
                },
                {
                    title: 'Alicia Keys - Fallin\'',
                    solution: '00s',
                    otherOption: '90s',
                },
                {
                    title: 'Avril Lavigne - Complicated',
                    solution: '00s',
                    otherOption: '90s',
                },
                {
                    title: 'Coolio - Gangsta\'s Paradise',
                    solution: '90s',
                    otherOption: '00s',
                },
                {
                    title: 'Celine Dion - My Heart Will Go On',
                    solution: '90s',
                    otherOption: '00s',
                },
                {
                    title: 'Backstreet Boys - I Want it That Way',
                    solution: '90s',
                    otherOption: '00s',
                },
                {
                    title: 'Fugees - Killing Me Softly',
                    solution: '90s',
                    otherOption: '00s',
                },
                {
                    title: 'Ricky Martin - Livin\' La Vida Loca',
                    solution: '90s',
                    otherOption: '00s',
                },
            ],
        });

        await insertQuiz(queryRunner, {
            title: 'Is it the Onion?',
            description: 'Sometimes it feels like reality emulates parody.',
            challenges: [
                {
                    title: 'Somali Militant Group Al-Shabaab Announces Ban On Single-Use Plastic Bags',
                    solution: 'Actual headline',
                    otherOption: 'The Onion',
                },
                {
                    title: 'France Is Running Out Of Butter For Its Croissants',
                    solution: 'Actual headline',
                    otherOption: 'The Onion',
                },
                {
                    title: 'Apology After Japanese Train Departs 20 Seconds Early',
                    solution: 'Actual headline',
                    otherOption: 'The Onion',
                },
                {
                    title: 'Alcohol Level In Air At Fraternity Party Registers On Breathalyzer ',
                    solution: 'Actual headline',
                    otherOption: 'The Onion',
                },
                {
                    title: 'Uber’s Search For A Female CEO Has Been Narrowed Down To 3 Men',
                    solution: 'Actual headline',
                    otherOption: 'The Onion',
                },
                {
                    title: 'Gay Conversion Therapists Claim Most Patients Fully Straight By The Time They Commit Suicide',
                    solution: 'The Onion',
                    otherOption: 'Actual headline',
                },
                {
                    title: 'Study Finds ‘Missionary,’ ‘In Love’ Most Popular Porn Search Terms',
                    solution: 'The Onion',
                    otherOption: 'Actual headline',
                },
                {
                    title: 'Buddy System Responsible For Additional Death',
                    solution: 'The Onion',
                    otherOption: 'Actual headline',
                },
                {
                    title: 'Evangelical Scientists Refute Gravity With New \'Intelligent Falling\' Theory',
                    solution: 'The Onion',
                    otherOption: 'Actual headline',
                },
                {
                    title: 'Johnson & Johnson Introduces \'Nothing But Tears\' Shampoo To Toughen Up Newborns',
                    solution: 'The Onion',
                    otherOption: 'Actual headline',
                },
            ],
        });
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

interface InitialQuiz {
    title: string;
    description: string;
    challenges: Array<{
        title: string;
        solution: string;
        otherOption: string;
    }>;
}

async function insertQuiz(queryRunner: QueryRunner, quiz: InitialQuiz) {
    quiz.title = quiz.title.replace(/'/g, "'");
    quiz.description = quiz.description.replace(/'/g, "''");
    const [ insertedQuiz ]: [ { id: number } ] = await queryRunner.query(
        `INSERT INTO quiz ("title", "description", "version") VALUES ('${quiz.title}', '${quiz.description}', 0) RETURNING "id"`,
    );

    await Promise.all(quiz.challenges.map(async (challenge) => {
        challenge.title = challenge.title.replace(/'/g, "''");
        challenge.solution = challenge.solution.replace(/'/g, "''");
        challenge.otherOption = challenge.otherOption.replace(/'/g, "''");
        const [ insertedChallenge ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${insertedQuiz.id}, '${challenge.title}', 0) RETURNING "id"`,
        );
        const [ insertedSolution ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${insertedChallenge.id}, '${challenge.solution}', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${insertedChallenge.id}, '${challenge.otherOption}', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${insertedSolution.id} WHERE "id"=${insertedChallenge.id}`,
        );
    }));
}
