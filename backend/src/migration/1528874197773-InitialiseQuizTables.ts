import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialiseQuizTables1528874197773 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "quiz" ("id" SERIAL NOT NULL, "title" text NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, CONSTRAINT "PK_422d974e7217414e029b3e641d0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "option" ("id" SERIAL NOT NULL, "title" text NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "challengeId" integer, CONSTRAINT "PK_e6090c1c6ad8962eea97abdbe63" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "challenge" ("id" SERIAL NOT NULL, "title" text NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "quizId" integer, "solutionId" integer, CONSTRAINT "REL_60428387a273b5773ba7aad851" UNIQUE ("solutionId"), CONSTRAINT "PK_5f31455ad09ea6a836a06871b7a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "option" ADD CONSTRAINT "FK_19413e09578f0fbfb311a178150" FOREIGN KEY ("challengeId") REFERENCES "challenge"("id")`);
        await queryRunner.query(`ALTER TABLE "challenge" ADD CONSTRAINT "FK_86968ca225cb39e5bcd760db7f6" FOREIGN KEY ("quizId") REFERENCES "quiz"("id")`);
        await queryRunner.query(`ALTER TABLE "challenge" ADD CONSTRAINT "FK_60428387a273b5773ba7aad851d" FOREIGN KEY ("solutionId") REFERENCES "option"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "challenge" DROP CONSTRAINT "FK_60428387a273b5773ba7aad851d"`);
        await queryRunner.query(`ALTER TABLE "challenge" DROP CONSTRAINT "FK_86968ca225cb39e5bcd760db7f6"`);
        await queryRunner.query(`ALTER TABLE "option" DROP CONSTRAINT "FK_19413e09578f0fbfb311a178150"`);
        await queryRunner.query(`DROP TABLE "challenge"`);
        await queryRunner.query(`DROP TABLE "option"`);
        await queryRunner.query(`DROP TABLE "quiz"`);
    }

}
