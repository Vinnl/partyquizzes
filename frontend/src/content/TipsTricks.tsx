import * as React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

export const TipsTricks: React.SFC = () => {
  return (<>
    <section className="section">
      <div className="container content">
        <Helmet>
          <title>Tips & tricks for creating a fun quiz · {process.env.REACT_APP_NAME}</title>
          <meta name="description" content="Creating a Party Quiz is not as simple as you might think. Our online quiz maker app makes the technical steps pretty easy, but it cannot provide you with the required creativity and ideas! That said, the following tips might provide some help for you to create the next best quiz."/>
        </Helmet>
        <h2 className="title">Tips & tricks for creating a fun quiz</h2>
        <p>Creating a Party Quiz is not as simple as you might think. Our online quiz maker app makes the technical steps pretty easy, but it cannot provide you with the required creativity and ideas! That said, the following tips might provide some help for you to create the next best quiz.</p>
        <h3 className="title">Quiz idea</h3>
        <p>The first step towards creating the perfect quiz to entertain your friends is coming up with the concept. In our experience, there are several types of concepts that make a quiz fun.</p>
        <h4 className="title is-size-6">Interesting trivia</h4>
        <p>Many players love to learn fun new facts. Thus, if there is a subject that you know more about than the average person, that could be the perfect starting point for an interesting quiz. For example, many people have a vague hunch of what Scientology is, but do not know their actual teachings. Thus, a quiz <b>Scientology or Actual Science</b> is a great vehicle for a quiz maker to drop fun facts about Scientology's beliefs, by comparing them to widely trusted facts in the scientific community.</p>
        <h4 className="title is-size-6">When reality meets parody</h4>
        <p>If you want to draw attention to something you consider ridiculous, you can compare it to something that is widely considered ridiculous. For example, some news headlines seem so far-fetched that they can easily be mistaken for parody. Before you know it, the quiz <b>The Onion or Actual Headline</b> is born!</p>
        <h3 className="title">Coming up with challenges</h3>
        <p>Coming up with the perfect concept for a quiz is one thing, but even the best concept doesn't write itself. Of course, for some concepts, it's obvious what the challenges should be. For others, not so much.</p>
        <p>For example, if you want to share interesting trivia, how do you determine which facts are interesting enough to mention? As always, <a href="https://www.wikipedia.org/" title="Wikipedia" target="_blank">Wikipedia</a> is a treasure trove of information. Even just seeing interesting facts laid out on a single page can help you come up with the right challenges to ask of your players.</p>
        <p>If your quiz revolves around a person. fictional or not, <a href="https://www.wikiquote.org/" title="Wikiquote" target="_blank">Wikiquote</a> is a fantastic source of quotes by or about this person. Perfect candidates to feature in your quiz!</p>
        <h3 className="title">Promoting your quiz</h3>
        <p>If your quiz is new, it will immediately be listed on <Link to="/" title="Home">the Party Quizzes homepage</Link>, waiting for eager players to try it out. A good way to gather feedback is to share the link to your quiz with your friends. And who knows — if they like it, they might pass it on to their friends!</p>
        <p>Another good way to reach a wide audience is to share it on social media. If you know of online communities that might find your quiz interesting or funny, those are the perfect place to reach an audience that both wants to play your quiz and might want to pass it on to their friends.</p>
        <h3 className="title">Just do it already!</h3>
        <p>
          <Link
            to="/quizzes/create"
            title="Create your own Party Quiz"
          >
            Create your quiz now.
          </Link>
        </p>

{/* ideas template online maker games your friends star app */}
      </div>
    </section>
  </>);
};
