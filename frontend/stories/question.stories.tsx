import * as React from 'react';

import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import { Question } from '../src/quiz/Question';

storiesOf('Question', module)
.add('Question', () => (
  <Question
    challenge='How much is the fish?'
    options={[ { title: 'Scooter lyrics' }, { title: 'AutoComplete'} ]}
    onAnswer={action('Answer')}
  />
));
