import * as React from 'react';
import { OutboundLink } from 'react-ga';

import './Sharer.css';

import { SharerProps } from './Sharer';

export const FacebookSharer: React.SFC<SharerProps> = (props) => (
  <aside className="box sharer">
    <p className="content">
      {props.message}
    </p>
    <div className="field">
      <div className="control has-text-right">
      <OutboundLink
        to={`https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(props.url)}&quote=${encodeURIComponent(props.message)}`}
        title="Share on Facebook"
        className="button facebook"
        target="_blank"
        eventLabel="share-facebook"
      >
        Share on Facebook
      </OutboundLink>
      </div>
    </div>
  </aside>
);
