variable "heroku_email" {}
variable "heroku_api_key" {}
variable "subdomain" {}

terraform {
  required_version = "~> 0.11.0"
  backend "s3" {
    bucket = "terraform-remote-state-partyquizzes.com"
    key = "terraform.tfstate"
    region = "eu-central-1"
    encrypt = true
  }
}

provider "heroku" {
  version = "~> 1.0"

  email = "${var.heroku_email}"
  api_key = "${var.heroku_api_key}"
}
provider "cloudflare" {
  version = "~> 1.0"
}

resource "heroku_app" "heroku_partyquizzes_app" {
  name = "pq-${terraform.workspace}"
  region = "eu"
  buildpacks = [
    "heroku/nodejs"
  ]

  config_vars {
    REACT_APP_NAME = "PartyQuizzes"
    PUBLIC_URL = "${var.subdomain == "www" ? "https://partyquizzes.com" : "https://${var.subdomain}.partyquizzes.com"}"
  }
}

resource "heroku_addon" "database" {
  app  = "${heroku_app.heroku_partyquizzes_app.name}"
  plan = "heroku-postgresql:hobby-dev"
}

resource "heroku_domain" "partyquizzes_heroku_vincenttunru_domain" {
  app      = "${heroku_app.heroku_partyquizzes_app.name}"
  hostname = "${cloudflare_record.partyquizzes_cloudflare_vincenttunru_record.hostname}"
}

resource "heroku_domain" "partyquizzes_heroku_vincenttunru_production_domain" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  app      = "${heroku_app.heroku_partyquizzes_app.name}"
  hostname = "${cloudflare_record.partyquizzes_cloudflare_vincenttunru_production_record.hostname}"
}

resource "heroku_domain" "partyquizzes_heroku_domain" {
  app      = "${heroku_app.heroku_partyquizzes_app.name}"
  hostname = "${cloudflare_record.partyquizzes_cloudflare_record.hostname}"
}

resource "heroku_domain" "partyquizzes_heroku_root_domain" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  app      = "${heroku_app.heroku_partyquizzes_app.name}"
  hostname = "${cloudflare_record.partyquizzes_cloudflare_root_record.hostname}"
}

resource "cloudflare_record" "partyquizzes_cloudflare_vincenttunru_record" {
  domain = "vincenttunru.com"
  name   = "${terraform.workspace}-review-partyquizzes"
  value  = "${heroku_app.heroku_partyquizzes_app.heroku_hostname}"
  type   = "CNAME"
  ttl    = 1
  proxied = true
}

resource "cloudflare_record" "partyquizzes_cloudflare_vincenttunru_production_record" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  domain = "vincenttunru.com"
  name   = "partyquizzes"
  value  = "${heroku_app.heroku_partyquizzes_app.heroku_hostname}"
  type   = "CNAME"
  ttl    = 1
  proxied = true
}

resource "cloudflare_record" "partyquizzes_cloudflare_record" {
  domain = "partyquizzes.com"
  name   = "${var.subdomain}"
  value  = "${heroku_app.heroku_partyquizzes_app.heroku_hostname}"
  type   = "CNAME"
  ttl    = 1
  proxied = true
}

resource "cloudflare_record" "partyquizzes_cloudflare_root_record" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  domain = "partyquizzes.com"
  name   = "partyquizzes.com"
  value  = "${heroku_app.heroku_partyquizzes_app.heroku_hostname}"
  type   = "CNAME"
  ttl    = 1
  proxied = true
}

# Note: these settings apply not just to this branch, but to everything on partyquizzes.com!
# That's why it's only active when working on the `www` subdomain (i.e. production).
resource "cloudflare_zone_settings_override" "partyquizzes_cloudflare_settings" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  name = "${cloudflare_record.partyquizzes_cloudflare_record.domain}"

  settings {
    tls_1_3 = "on"
    always_use_https = "on"
    ssl = "full"
    # Make sure Tor users do not have to enter a CAPTCHA every time:
    security_level = "essentially_off"
  }
}

# Note: these page rules apply not just to this branch, but to everything on partyquizzes.com!
# That's why it's only active when working on the `www` subdomain (i.e. production).
resource "cloudflare_page_rule" "partyquizzes_cloudflare_page_rules_no-www" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  zone = "${cloudflare_record.partyquizzes_cloudflare_record.domain}"
  target = "*www.${cloudflare_record.partyquizzes_cloudflare_record.domain}/*"
  priority = 10

  actions = {
    forwarding_url = {
      url = "https://${cloudflare_record.partyquizzes_cloudflare_record.domain}/$2",
      status_code = 301
    }
  }
}

# Note: these page rules apply not just to this branch, but to everything on partyquizzes.com!
# That's why it's only active when working on the `www` subdomain (i.e. production).
resource "cloudflare_page_rule" "partyquizzes_cloudflare_page_rules" {
  count = "${var.subdomain == "www" ? 1 : 0}"
  zone = "${cloudflare_record.partyquizzes_cloudflare_record.domain}"
  target = "http://*${cloudflare_record.partyquizzes_cloudflare_record.domain}/*"
  priority = 20

  actions = {
    # With the always_use_https action present, CloudFlare does not allow addition actions:
    always_use_https = true,
  }
}

output "heroku_git_url" {
  value = "${heroku_app.heroku_partyquizzes_app.git_url}"
}
output "heroku_hostname" {
  value = "${heroku_app.heroku_partyquizzes_app.heroku_hostname}"
}

