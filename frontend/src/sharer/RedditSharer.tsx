import * as React from 'react';
import { OutboundLink } from 'react-ga';

import './Sharer.css';

import { SharerProps } from './Sharer';

export const RedditSharer: React.SFC<SharerProps> = (props) => (
  <aside className="box sharer">
    <p className="content">
      {props.message}
    </p>
    <div className="field">
      <div className="control has-text-right">
      <OutboundLink
        to={`https://www.reddit.com/submit?url=${encodeURIComponent(props.url)}&title=${encodeURIComponent(props.message)}`}
        title="Share on Reddit"
        className="button reddit"
        target="_blank"
        eventLabel="share-reddit"
      >
        Share on Reddit
      </OutboundLink>
      </div>
    </div>
  </aside>
);
