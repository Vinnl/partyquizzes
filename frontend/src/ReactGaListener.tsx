/*
 * This component should be added inside a Router component from React Router.
 * It will listen to route changes and send them to Google Analytics.
 */

import * as PropTypes from 'prop-types';
import * as React from 'react';
import * as ReactGA from 'react-ga';

// tslint:disable:object-literal-sort-keys
const gaDimensions = {
  GA_CONFIG_VERSION: 'dimension1',
  CODE_BRANCH: 'dimension2',
};
// tslint:enable:object-literal-sort-keys

if (
  (typeof navigator !== 'undefined' && typeof window !== 'undefined') &&
  (!navigator.doNotTrack || navigator.doNotTrack.substr(0, 1) !== '1') &&
  (!window.doNotTrack || window.doNotTrack.substr(0, 1) !== '1')
) {
  ReactGA.initialize('UA-121426507-2');
  ReactGA.set({
    // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#loading-analyticsjs
    transport: 'beacon',
    // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#tracking-custom-data
    [gaDimensions.GA_CONFIG_VERSION]: '1',
    [gaDimensions.CODE_BRANCH]: process.env.REACT_APP_CODE_BRANCH,
  });
}

export class ReactGAListener extends React.Component<{}, {}> {
  // React Router passes the router down through React's "context" API:
  // https://facebook.github.io/react/docs/context.html
  // We need to access that to be able to listen to route changes globally.
  public static contextTypes = {
    router: PropTypes.object,
  };

  public componentDidMount() {
    sendPageView(this.context.router.history.location);
    this.context.router.history.listen(sendPageView);
  }

  public render() {
    return <div>{this.props.children}</div>;
  }
}

function sendPageView(location: Location) {
  ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
}
