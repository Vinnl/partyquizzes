export interface Option {
  id?: number;
  title: string;
}
export interface InitialisedOption extends Option {
  id: number;
}

export interface Challenge {
  challenge: string;
  options: [Option, Option];
  solution: number;
}
export interface InitialisedChallenge extends Challenge {
  options: [InitialisedOption, InitialisedOption];
}

export interface Quiz {
  id?: number;
  title: string;
  challenges: Challenge[];
  description?: string;
}

export interface InitialisedQuiz extends Quiz {
  id: number;
  challenges: InitialisedChallenge[];
}

export type GetQuizResponse = InitialisedQuiz;

export type GetQuizzesResponse = Array<{
  createdAt: string;
  id: number;
  title: string;
  updatedAt: string;
  description?: string;
}>;

export interface PostQuizResultRequest {
  quizId: number;
  answers: number[];
}

export type PostQuizRequest = Quiz;
export interface PostQuizResponse {
  id: number;
}

export function isValidQuiz(proposedQuiz: Partial<Quiz>): proposedQuiz is Quiz {
  return typeof proposedQuiz === 'object'
    && typeof proposedQuiz.title === 'string'
    && proposedQuiz.title.length > 0
    && Array.isArray(proposedQuiz.challenges)
    && proposedQuiz.challenges.length > 0;
}
