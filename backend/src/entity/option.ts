import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Challenge } from './challenge';
import { QuizAnswer } from './quizAnswers';

@Entity('option')
export class Option {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column('text')
  public title: string;

  @ManyToOne(type => Challenge, challenge => challenge.options, { onDelete: 'CASCADE' })
  public challenge: Challenge;

  @OneToMany(type => QuizAnswer, answer => answer.option)
  public answers: QuizAnswer[];

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @VersionColumn()
  public version: number;
}
