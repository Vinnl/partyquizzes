import * as React from 'react';

import { storiesOf } from '@storybook/react';

import { QuizCreator } from '../src/quizCreator/QuizCreator';

storiesOf('QuizCreator', module)
.add('Empty QuizCreator', () => (
  <QuizCreator/>
));
