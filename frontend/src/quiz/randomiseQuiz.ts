import { GetQuizResponse, InitialisedOption } from "../lib/endpoints/quiz";

export const randomiseQuiz = (quiz: GetQuizResponse) => {
  const [ challenges ] = shuffleArray(quiz.challenges);

  const randomisedChallenges = challenges.map(challenge => {
    const isSorted = challenge.options[0].title.localeCompare(challenge.options[1].title) < 0;

    const sortedOptions: [InitialisedOption, InitialisedOption] = isSorted
      ? challenge.options
      : [ challenge.options[1], challenge.options[0] ]
    const solution = isSorted ? challenge.solution : 1 - challenge.solution;

    return {
      ...challenge,
      options: sortedOptions,
      solution,
    };
  });

  const randomisedQuiz: GetQuizResponse = {
    ...quiz,
    challenges: randomisedChallenges,
  };

  return randomisedQuiz;
}

function shuffleArray<T>(sourceArray: T[]): [ T[], number[] ] {
  const outputArray = [...sourceArray];
  const originalMap: number[] = sourceArray.map((_, index) => index);

  for(let i = outputArray.length - 1; i > 0; i--) {
    const indexToSwitchWith = Math.floor(Math.random() * (i + 1));
    [outputArray[i], outputArray[indexToSwitchWith]] = [outputArray[indexToSwitchWith], outputArray[i]];

    [
      originalMap[originalMap[i]],
      originalMap[originalMap[indexToSwitchWith]]
    ] =
      [
        originalMap[originalMap[indexToSwitchWith]],
        originalMap[originalMap[i]]
      ];
  }

  return [outputArray, originalMap];
}
