import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Option } from './option';
import { Quiz } from './quiz';

@Entity('challenge')
export class Challenge {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column('text')
  public title: string;

  @ManyToOne(type => Quiz, quiz => quiz.challenges, { onDelete: 'CASCADE' })
  public quiz: Quiz;

  @OneToMany(type => Option, option => option.challenge)
  public options: Option[];

  @OneToOne(type => Option)
  @JoinColumn()
  public solution: Option;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @VersionColumn()
  public version: number;
}
