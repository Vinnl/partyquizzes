import * as express from 'express';
import * as fs from 'fs';
import * as path from 'path';
import { promisify } from 'util';
import { Connection } from 'typeorm';
import * as pathToRegexp from 'path-to-regexp';

import { initialiseDatabase } from './database';
import { newQuizHandler } from './endpoints/newQuiz';
import { newQuizResultHandler } from './endpoints/newQuizResult';
import { quizHandler, getQuiz } from './endpoints/quiz';
import { quizzesHandler, getQuizzes, quizFeedHandler } from './endpoints/quizzes';
import { InjectedData } from '../../frontend/src/lib/endpoints/injectedData';
import { URL } from 'url';

const initialise = async () => {
  const connection: Connection | Error = await initialiseDatabase();

  if (connection instanceof Error) {
    // tslint:disable-next-line:no-console
    console.error('Failed to initalise due to a database error', connection);

    return;
  }

  const app = express();

  app.use(express.json());

  app.get('/api/quizzes', quizzesHandler);
  app.post('/api/quizzes', newQuizHandler);
  app.get('/api/quizzes/:quizId', quizHandler);
  app.post('/api/answers', newQuizResultHandler);

  app.get('/feed/quizzes/new', quizFeedHandler);

  if (process.env.NODE_ENV === 'production') {
    app.get('/service-worker.js', (req, res) => {
      res.sendFile(path.join(__dirname, '../../compiled_frontend/service-worker.js'), { magAge: 0 });
    })
    // In production, this app will have been compiled into a `dist` folder.
    // The front end will have been built and copied into a `compiled_frontend` folder located inside it.
    app.use(
      express.static(
        path.join(__dirname, '../../compiled_frontend'),
        { index: false },
      ),
    );

    app.get('/*', async (req, res) => {
      const indexFile = await promisify(fs.readFile)(path.join(__dirname, '../../compiled_frontend', 'index.html'), 'utf8');
      const dataToInject: InjectedData = {};
      const metaTagsToInject: { [ key: string ]: string } = {};
      let pageTitle: string | undefined;

      if (req.path === '/') {
        const topQuizzes = await getQuizzes({ orderBy: 'top', limit: 5 });
        const newestQuizzes = await getQuizzes({ orderBy: 'new', limit: 5 });

        if (topQuizzes !== null) {
          dataToInject['/api/quizzes?limit=5&order=top'] = topQuizzes;
        }
        if (newestQuizzes !== null) {
          dataToInject['/api/quizzes?limit=5&order=new'] = newestQuizzes;
        }
      }

      const quizRe = pathToRegexp('/quiz/:quizId');
      if (quizRe.test(req.path)) {
        const quizId = parseInt(quizRe.exec(req.path)![1], 10);

        const quiz = await getQuiz(quizId);

        if (quiz !== null) {
          pageTitle = quiz.title;
          dataToInject[`/api/quizzes/${quizId}`] = quiz;
          metaTagsToInject['og:type'] = 'website';
          metaTagsToInject['og:title'] = 'Do the quiz: ' + quiz.title;
          metaTagsToInject['og:image'] = process.env.PUBLIC_URL! + '/favicon.png';
          metaTagsToInject['og:url'] = `${process.env.PUBLIC_URL!}/quiz/${quiz.id}`;
          metaTagsToInject['og:site_name'] = process.env.REACT_APP_NAME!;
          metaTagsToInject['twitter:card'] = 'summary';
          metaTagsToInject['twitter:title'] = 'Do the quiz: ' + quiz.title;
          if (quiz.description) {
            metaTagsToInject['description'] = quiz.description;
            metaTagsToInject['twitter:description'] = quiz.description;
          }
        }
      }

      const referrerUrl = req.get('Referer');
      if (referrerUrl) {
        const referrer = new URL(referrerUrl);

        if (referrer.hostname === 'facebook.com' || referrer.hostname === 'fb.me') {
          dataToInject.referrer = 'facebook';
        }
        if (referrer.hostname === 'twitter.com' || referrer.hostname === 't.co') {
          dataToInject.referrer = 'twitter';
        }
        if (referrer.hostname === 'reddit.com') {
          dataToInject.referrer = 'reddit';
        }
      }

      if (!metaTagsToInject['og:type']) {
        metaTagsToInject['og:type'] = 'website';
        metaTagsToInject['og:title'] = process.env.REACT_APP_NAME!;
        metaTagsToInject['og:image'] = process.env.PUBLIC_URL! + '/favicon.png';
        metaTagsToInject['og:url'] = process.env.PUBLIC_URL!;
      }
      if (!metaTagsToInject['twitter:card']) {
        metaTagsToInject['twitter:card'] = 'summary';
        metaTagsToInject['twitter:title'] = process.env.REACT_APP_NAME!;
      }

      const metaTags = Object.entries(metaTagsToInject)
        .map(([name, content]) => `<meta name="${name}" content="${content.replace(/"/g, '&quot;')}">`)
        .join('');

      // `$1` refers to the title found in the index file
      // (see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace#Specifying_a_string_as_a_parameter),
      // so if no custom pageTitle is set, we just keep it as-is.
      const newTitleElement = pageTitle
        ? `<title>${pageTitle} · $1</title>`
        : '<title>$1</title>';

      const preloadedIndexFile = indexFile.replace(
        'document.__INJECTED_DATA={}',
        `document.__INJECTED_DATA=${JSON.stringify(dataToInject)}`,
      )
      .replace(
        '<meta name="__INJECTED" content="">',
        metaTags,
      )
      .replace(/<title>(\w.+)<\/title>/, newTitleElement);

      res.send(preloadedIndexFile);
    });
  }

  const port = process.env.PORT || 5000;
  app.listen(port);
}

initialise();
