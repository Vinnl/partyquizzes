import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialiseFirstQuiz1528874229917 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const [ quiz ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO quiz ("title", "version") VALUES ('George W. Bush or Captain America?', 0) RETURNING "id"`,
        );

        const [ challenge1 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'We will rid the world of the evil-doers.', 0) RETURNING "id"`,
        );
        const [ solution1 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge1.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge1.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution1.id} WHERE "id"=${challenge1.id}`,
        );

        const [ challenge2 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'We have a place, all of us, in a long story. A story we continue, but whose end we will not see.', 0) RETURNING "id"`,
        );
        const [ solution2 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge2.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge2.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution2.id} WHERE "id"=${challenge2.id}`,
        );

        const [ challenge3 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'For Truth, Justice, and the American way!', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge3.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        const [ solution3 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge3.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution3.id} WHERE "id"=${challenge3.id}`,
        );

        const [ challenge4 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'This nation was founded on one principle above all else: the requirement that we stand up for what we believe, no matter the odds or the consequences.', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge4.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        const [ solution4 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge4.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution4.id} WHERE "id"=${challenge4.id}`,
        );

        const [ challenge5 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'This will be a monumental struggle of good versus evil. But good will prevail.', 0) RETURNING "id"`,
        );
        const [ solution5 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge5.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge5.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution5.id} WHERE "id"=${challenge5.id}`,
        );

        const [ challenge6 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'Believe in your country, but believe in yourself!', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge6.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        const [ solution6 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge6.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution6.id} WHERE "id"=${challenge6.id}`,
        );

        const [ challenge7 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'Americans are generous and strong and decent, not because we believe in ourselves, but because we hold beliefs beyond ourselves.', 0) RETURNING "id"`,
        );
        const [ solution7 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge7.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge7.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution7.id} WHERE "id"=${challenge7.id}`,
        );

        const [ challenge8 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'I know I''m asking a lot, but the price of freedom is high; it always has been. And it''s a price I''m willing to pay.', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge8.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        const [ solution8 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge8.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution8.id} WHERE "id"=${challenge8.id}`,
        );

        const [ challenge9 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'What are you ladies waiting for, Christmas?! ', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge9.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        const [ solution9 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge9.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution9.id} WHERE "id"=${challenge9.id}`,
        );

        const [ challenge10 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO challenge ("quizId", "title", "version") VALUES (${quiz.id}, 'To retreat before victory would be an act of recklessness and dishonor and I will not allow it.', 0) RETURNING "id"`,
        );
        const [ solution10 ]: [ { id: number } ] = await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge10.id}, 'George W. Bush', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `INSERT INTO option ("challengeId", "title", "version") VALUES (${challenge10.id}, 'Captain America', 0) RETURNING "id"`,
        );
        await queryRunner.query(
            `UPDATE challenge SET "solutionId"=${solution10.id} WHERE "id"=${challenge10.id}`,
        );

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
