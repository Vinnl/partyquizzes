import * as React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

import { DataFetcher, DataFetcherProps } from './DataFetcher';
import { GetQuizzesResponse } from './lib/endpoints/quiz';
import { QuizList } from './QuizList';

type Props = DataFetcherProps<GetQuizzesResponse>;

export const LandingPage: React.SFC<Props> = () => {
  return (<>
    <Helmet>
      <title>{process.env.REACT_APP_NAME}</title>
      <meta name="description" content="A quiz is the ultimate party game! Fun for adults, for kids, for teenagers, … For every one! Test your or your friends' knowledge by answering questions on a variety of topics, or make your own quiz using our quiz maker."/>
    </Helmet>
    <section className="section is-hidden-touch">
      <div className="container content">
        <div className="columns">
          <p className="column is-three-quarters-widescreen is-size-5-desktop">
            A quiz is the ultimate party game! Fun for adults, for kids, for teenagers, &hellip; For every one!
            Test your or your friends' knowledge by answering questions on a variety of topics, or&nbsp;
            <Link to="/quizzes/create" title="Create your own party quiz">
              make your own quiz using our quiz maker
            </Link>
            .
          </p>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <div className="columns is-desktop">
          <div className="column">
            <h2 className="title is-size-2 is-size-3-touch">Top quizzes</h2>
            <DataFetcher url="/api/quizzes?limit=5&order=top">
              <QuizList/>
            </DataFetcher>
          </div>
          <hr className="is-hidden-desktop"/>
          <div className="column is-offset-one-fifth-desktop">
            <h2 className="title is-size-4">Newest quizzes</h2>
            <DataFetcher url="/api/quizzes?limit=5&order=new">
              <QuizList size="small"/>
            </DataFetcher>
          </div>
        </div>
      </div>
    </section>
  </>);
};
