import * as LocalForage from 'localforage';
import * as React from 'react';
import * as ReactGA from 'react-ga';
import { Helmet } from 'react-helmet';
import { RouteComponentProps } from 'react-router';

import './Quiz.css';

import { DataFetcher, DataFetcherProps } from '../DataFetcher';
import { ErrorPage } from '../ErrorPage';
import { GetQuizResponse, InitialisedOption, PostQuizResultRequest } from '../lib/endpoints/quiz';
import { QuizScores } from '../lib/localForageInterfaces';
import { LoadingPage } from '../LoadingPage';
import { Sharer } from '../sharer/Sharer';
import { Question } from './Question';
import { QuizSuggestion } from './QuizSuggestion';
import { randomiseQuiz } from './randomiseQuiz';

interface RouteParams {
  id: string;
}
type Props = DataFetcherProps<GetQuizResponse> & {};

interface State {
  readonly currentChallenge: number;
  readonly answers: number[];
  readonly quizScores?: QuizScores;
}

type Challenges = Array<{
  challenge: string;
  options: [ InitialisedOption, InitialisedOption ];
  solution: number,
}>;

class BareQuiz extends React.Component<Props, State> {
  public readonly state: State = {
    answers: [],
    currentChallenge: 0,
  };

  public constructor(props: Props) {
    super(props);

    this.onAnswer = this.onAnswer.bind(this);
  }

  public async componentDidMount() {
    const quizScores = await LocalForage.getItem<QuizScores>('quizScores');

    if (quizScores) {
      this.setState({ quizScores });
    }
  }

  public render() {
    if (typeof this.props.data === 'undefined') {
      return (
        <LoadingPage>
          Loading...
        </LoadingPage>
      );
    }
    if (this.props.data === null) {
      return (
        <ErrorPage>
          There was a problem loading quiz data, please try again.
        </ErrorPage>
      );
    }

    const challenges = this.props.data.challenges;

    if (this.state.currentChallenge >= challenges.length) {
      return this.renderResults(challenges);
    }

    return (
      <div itemScope={true} itemType="https://schema.org/Game">
        <section className="section">
          <div className="container">
            <Helmet>
              <title>{this.props.data.title} · {process.env.REACT_APP_NAME}</title>
              {this.props.data.description ? <meta name="description" content={this.props.data.description}/> : null}
            </Helmet>
            <h2 itemProp="name" className="title is-size-3-widescreen is-size-4">
              {this.props.data.title}
            </h2>
            {this.props.data.description ? <p>{this.props.data.description}</p> : null}
            <hr/>
            <Question
              challenge={challenges[this.state.currentChallenge].challenge}
              options={challenges[this.state.currentChallenge].options}
              onAnswer={this.onAnswer}
              key={this.state.currentChallenge}
            />
            <p>
              <progress
                className="progress is-small is-info"
                value={this.state.currentChallenge + 1}
                max={challenges.length}
              >
                Challenge {this.state.currentChallenge + 1} / {challenges.length}
              </progress>
            </p>
          </div>
        </section>
      </div>
    );
  }

  private onAnswer(selectedOptionIndex: number) {
    this.setState(
      (prevState) => {
        const answers = [ ...prevState.answers ];
        answers[prevState.currentChallenge] = selectedOptionIndex;

        return {
          answers,
          currentChallenge: prevState.currentChallenge + 1,
        };
      },
      async () => {
        ReactGA.event({
          action: 'Answer',
          category: 'Quiz',
        });

        const quiz = this.props.data;
        if (!quiz || this.state.currentChallenge !== quiz.challenges.length) {
          return;
        }

        // When the quiz is finished, send the given answers to the back-end for statistics...
        const headers = new Headers({
          'Content-Type': 'application/json',
        });
        const request: PostQuizResultRequest = {
          answers: this.state.answers.map((answerIndex, challengeIndex) => quiz.challenges[challengeIndex].options[answerIndex].id),
          quizId: quiz.id,
        };
        fetch('/api/answers', {
          body: JSON.stringify(request),
          headers,
          method: 'POST',
        });

        // ...and store locally that the user has completed this quiz
        const quizScores = await LocalForage.getItem<QuizScores>('quizScores') || [];
        const score = this.calculateScore(quiz.challenges, this.state.answers);
        if (!quizScores[quiz.id] || quizScores[quiz.id].score < score) {
          quizScores[quiz.id] = {
            score,
            total: quiz.challenges.length,
          }
          LocalForage.setItem('quizScores', quizScores);
        }

        ReactGA.event({
          action: 'Finish',
          category: 'Quiz',
        });
      }
    );
  }

  private renderResults(challenges: Challenges) {
    return (
      <>
        {this.renderScore(challenges)}
        {this.renderSuggestedQuiz()}
        {this.renderAnswers(challenges)}
      </>
    );
  }

  private renderScore(challenges: Challenges) {
    const score: number = this.calculateScore(challenges, this.state.answers);

    const titlePrefix = (this.props.data) ? this.props.data.title + ' — ' : '';

    if (score === 0) {
      return this.getScoreElement(
        'Ouch…',
        `You scored ${score} out of ${challenges.length} — better luck next time!`,
        `${titlePrefix}I scored ${score} out of ${challenges.length}… Can you do better?`,
      );
    }

    if (score === challenges.length) {
      return this.getScoreElement(
        'Perfect!',
        `You achieved the perfect score of ${score} out of ${challenges.length}!`,
        `${titlePrefix}I scored ${score} out of ${challenges.length}. Can you do the same?`,
      );
    }

    if (score < 3 && challenges.length > 5) {
      return this.getScoreElement(
        'Practice makes perfect',
        `You scored ${score} out of ${challenges.length} — not the worst, but could be better.`,
        `${titlePrefix}I scored ${score} out of ${challenges.length}. Can you do better?`,
      );
    }

    if (score < (challenges.length / 2)) {
      return this.getScoreElement(
        'Not bad',
        `You scored ${score} out of ${challenges.length} — not too shabby.`,
        `${titlePrefix}I scored ${score} out of ${challenges.length}. Can you do better?`,
      );
    }

    return this.getScoreElement(
      'Good job!',
      `You scored ${score} out of ${challenges.length}!`,
      `${titlePrefix}I scored ${score} out of ${challenges.length}. Can you beat me?`,
    );
  }

  private renderSuggestedQuiz() {
    const exclude: number[] = (this.state.quizScores)
      ? Object.keys(this.state.quizScores).map((quizId) => parseInt(quizId, 10))
      : [];

    if (this.props.data) {
      exclude.push(this.props.data.id);
    }

    const url = '/api/quizzes?limit=1&exclude=' + exclude.join(',');

    return (
      <section className="section">
        <div className="container is-size-4">
          <DataFetcher url={url}>
            <QuizSuggestion/>
          </DataFetcher>
        </div>
      </section>
    );
  }

  private getScoreElement(title: string, text: string, shareText: string) {
    return (
      <section className="section">
          <div className="container content">
            <div className="columns">
              <div className="column">
                <h2 className="title is-size-2">{title}</h2>
                {text}
              </div>
              <div className="column">
                <Sharer url={document.location.toString()} message={shareText}/>
              </div>
            </div>
          </div>
        </section>
    );
  }

  private renderAnswers(challenges: Challenges) {
    const answers = this.state.answers.map((answer, challengeIndex) => (
      <tr key={`answerTo${challengeIndex}`} title={'You answered: ' + challenges[challengeIndex].options[answer].title}>
        <td className="is-hidden-mobile">
          { answer === challenges[challengeIndex].solution
            ? <p className="tag is-success">Correct</p>
            : <p className="tag is-warning">Incorrect</p>
          }
        </td>
        <td>
          <p>{challenges[challengeIndex].challenge}</p>
        </td>
        <td>
          <p>
            {challenges[challengeIndex].options[challenges[challengeIndex].solution].title}
          </p>
        </td>
      </tr>
    ));

    return (
      <section className="section">
        <div className="container">
          <h2 className="title">Answers</h2>
          <table className="table is-fullwidth is-striped">
            <thead>
              <tr>
                <th className="is-hidden-mobile"/>
                <th>Challenge</th>
                <th>Correct answer</th>
              </tr>
            </thead>
            <tbody>
              {answers}
            </tbody>
          </table>
        </div>
      </section>
    );
  }

  private calculateScore(challenges: Challenges, answers: number[]) {
    const score: number = answers.reduce(
      (scoreSoFar: number, answer: number, challengeIndex: number) => {
        return (answer === challenges[challengeIndex].solution) ? scoreSoFar + 1 : scoreSoFar;
      },
      0,
    );

    return score;
  }
}

export const Quiz: React.SFC<RouteComponentProps<RouteParams>> = (props) => (
  <DataFetcher
    url={`/api/quizzes/${props.match.params.id}`}
    processData={randomiseQuiz}
  >
    <BareQuiz/>
  </DataFetcher>
);
