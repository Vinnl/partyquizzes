import * as React from 'react';

import './Question.css';

import { Option } from '../lib/endpoints/quiz';
import { OptionButton } from './OptionButton';

interface Props {
  challenge: string;
  options: [ Option, Option ];
  onAnswer: (selectedOption: number) => void;
}

export const Question: React.SFC<Props> = (props) => {
  return (
    <div itemScope={true} itemType="https://schema.org/Question" className="columns">
      <div className="column">
          <div itemProp="name" className="is-size-3-widescreen is-size-4">
            {props.challenge}
          </div>
      </div>
      <div className="column is-narrow">
        <div className="buttons optionButtons">
          {renderOptions(props.options, props.onAnswer)}
        </div>
      </div>
    </div>
  );
};

function renderOptions(
  options: [ Option, Option ],
  onAnswer: (selectedOption: number) => void,
): React.ReactNode {
  return options.map((option, index) => (
    <OptionButton
      key={'option_' + index}
      quote={option.title}
      optionIndex={index}
      onAnswer={onAnswer}
    />
  ));
}