import * as React from 'react';

import { storiesOf } from '@storybook/react';

import { FacebookSharer } from '../src/sharer/FacebookSharer';
import { RedditSharer } from '../src/sharer/RedditSharer';
import { TwitterSharer } from '../src/sharer/TwitterSharer';

storiesOf('Sharer', module)
.add('Minimal RedditSharer', () => (
  <RedditSharer
    message="Hipster or Hobo? I scored 7/10, can you beat me?"
    url="https://partyquizzes.com/quiz/1"
  />
))
.add('RedditSharer with tags', () => (
  <RedditSharer
    message="Hipster or Hobo? I scored 7/10, can you beat me?"
    url="https://partyquizzes.com/quiz/1"
    tags={['tag one', 'tag two']}
  />
))
.add('Minimal TwitterSharer', () => (
  <TwitterSharer
    message="Hipster or Hobo? I scored 7/10, can you beat me?"
    url="https://partyquizzes.com/quiz/1"
  />
))
.add('TwitterSharer with tags', () => (
  <TwitterSharer
    message="Hipster or Hobo? I scored 7/10, can you beat me?"
    url="https://partyquizzes.com/quiz/1"
    tags={['tag one', 'tag two']}
  />
))
.add('Minimal FacebookSharer', () => (
  <FacebookSharer
    message="Hipster or Hobo? I scored 7/10, can you beat me?"
    url="https://partyquizzes.com/quiz/1"
  />
))
.add('FacebookSharer with tags', () => (
  <FacebookSharer
    message="Hipster or Hobo? I scored 7/10, can you beat me?"
    url="https://partyquizzes.com/quiz/1"
    tags={['tag one', 'tag two']}
  />
));
