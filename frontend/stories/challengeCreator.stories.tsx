import * as React from 'react';

import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import { ChallengeCreator } from '../src/quizCreator/ChallengeCreator';

storiesOf('ChallengeCreator', module)
.add('New challenge', () => <ChallengeCreator id={0} onComplete={action('Submitted')}/>)
.add('Configured challenge', () => (
  <ChallengeCreator
    id={0}
    onComplete={action('Submitted')}
    challenge={{
      otherOptions: [ 'AutoComplete' ],
      solution: 'Scooter lyrics',
      title: 'How much is the fish?',
    }}
  />
));
