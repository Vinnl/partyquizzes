import * as React from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';

import { DataFetcher, DataFetcherProps } from './DataFetcher';
import { ErrorPage } from './ErrorPage';
import { GetQuizzesResponse } from './lib/endpoints/quiz';
import { LoadingPage } from './LoadingPage';

type Props = DataFetcherProps<GetQuizzesResponse>;

const BareRandomQuiz: React.SFC<Props> = (props) => {
    if (typeof props.data === 'undefined') {
      return (
        <LoadingPage>
          Loading...
        </LoadingPage>
      );
    }
    if (props.data === null) {
      return (
        <ErrorPage>
          There was a problem loading a quiz, please try again.
        </ErrorPage>
      );
    }

  return (
    <Redirect
      to={`/quiz/${props.data[0].id}`}
    />
  );
};

export const RandomQuiz: React.SFC<RouteComponentProps<{}>> = (props) => (
  <DataFetcher
    url={'/api/quizzes/?order=random&limit=1'}
  >
    <BareRandomQuiz/>
  </DataFetcher>
);
