import * as React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

import './FAQ.css';

// tslint:disable:object-literal-sort-keys
const faqSections = [
  {
    title: 'General',
    faqs: [
      {
        q: 'Why the name, "Party Quizzes"?',
        a: 'Quizzes are fun to play, not just by yourself, but also together with your friends. As such, we really do consider our quizzes to be a party game. Of course, it also helped that partyquizzes.com was still available.',
      },
      {
        q: 'Can I see which quizzes I have already completed?',
        a: 'Yes, quizzes you have already completed are accompanied by a small badge showing the best score you achieved on that quiz so far.',
      },
      {
        q: 'Do I need an account to play quizzes?',
        a: 'No. All Party Quizzes functionality is available to every one without requiring registration.',
      },
      {
        q: 'Is there a Party Quizzes app?',
        a: 'Not yet. However, in most modern mobile browsers you can add the Party Quizzes website to your homescreen. Quizzes you have already played will even work offline.',
      },
    ],
  },
  {
    title: 'Making quizzes with the quiz creator',
    faqs: [
      {
        q: 'Can I create a quiz about anything I want?',
        a: 'At the moment there are no restrictions on the contents of quizzes, though unlawful ones will of course be removed. We might retroactively revise this if this laissez-faire approach turns out to be problematic though, sorry.',
      },
      {
        q: 'What makes a quiz a good quiz?',
        a: <>If you need help creating interesting quizzes, see our <Link to="/tips-tricks" title="Tips & tricks for creating a quiz">tips & tricks</Link>.</>,
      },
      {
        q: 'Do I need an account to make quizzes?',
        a: 'No. All Party Quizzes functionality is available to every one without require registration.',
      },
      {
        q: 'Can I see how many people have played my quiz?',
        a: 'Not yet. Working on it, though.',
      },
      {
        q: 'Can I edit a quiz I created?',
        a: 'Unfortunately, no. Since we do not track our users, we cannot know whether you are the actual author of a quiz. You can create a new quiz though.',
      },
      {
        q: 'Can a challenge have more than two answers?',
        a: <>At the moment, no. If this is something you would like to do, let us know at <a href="mailto:feedback@partyquizzes.com" title="Send your feedback via email">feedback@partyquizzes.com</a>.</>,
      },
      {
        q: 'Can I use images as challenges or answers?',
        a: <>At the moment, no. If this is something you would like to do, let us know at <a href="mailto:feedback@partyquizzes.com" title="Send your feedback via email">feedback@partyquizzes.com</a>.</>,
      },
    ],
  }
];
// tslint:enable:object-literal-sort-keys

export const Faq: React.SFC = () => {
  return (<>
    <header className="section">
      <div className="container content">
        <Helmet>
          <title>Frequently Asked Questions · {process.env.REACT_APP_NAME}</title>
          <meta name="description" content="Have a question about Party Quizzes? It is probably answered as one of the Frequently Asked Questions."/>
        </Helmet>
        <h2 className="title">Frequently Asked Questions</h2>
        <p>Have a question about Party Quizzes? It is probably answered below. If not, feel free to send your questions to <a href="mailto:questions@partyquizzes.com" title="Email a question about Party Quizzes">questions@partyquizzes.com</a>.</p>
      </div>
    </header>
    {renderSections(faqSections)}
  </>);
};

function renderSections(sections: typeof faqSections): React.ReactNode {
  return sections.map((section, sectionIndex) => {
    const faqs = section.faqs.map((qanda, qandaIndex) => (
      <React.Fragment key={`qanda${qandaIndex}`}>
        <dt>{qanda.q}</dt>
        <dd>{qanda.a}</dd>
      </React.Fragment>
    ));

    return (
      <section key={`section${sectionIndex}`} className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-half-widescreen content">
              <h3 className="title is-size-5">{section.title}</h3>
              <dl className="faqs">{faqs}</dl>
            </div>
          </div>
        </div>
      </section>
    );
  });
}
