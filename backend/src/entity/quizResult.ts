import { CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from 'typeorm';

import { Quiz } from './quiz';
import { QuizAnswer } from './quizAnswers';

@Entity('quizResult')
export class QuizResult {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => Quiz, quiz => quiz.results, { onDelete: 'CASCADE' })
  public quiz: Quiz;

  @OneToMany(() => QuizAnswer, quizAnswer => quizAnswer.quizResult)
  public answers: QuizAnswer[];

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @VersionColumn()
  public version: number;
}
