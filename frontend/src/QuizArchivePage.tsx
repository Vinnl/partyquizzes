import * as React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';

import { DataFetcher, DataFetcherProps } from './DataFetcher';
import { GetQuizzesResponse } from './lib/endpoints/quiz';
import { QuizList } from './QuizList';

type Props = DataFetcherProps<GetQuizzesResponse>;

export const QuizArchivePage: React.SFC<Props> = () => {
  return (<>
    <section className="section is-hidden-touch">
      <div className="container content">
        <div className="columns">
          <p className="column is-three-quarters-widescreen is-size-5-desktop">
            <Helmet>
              <title>All quizzes · {process.env.REACT_APP_NAME}</title>
              <meta name="description" content="A list of all currently available Party Quizzes."/>
            </Helmet>
            A list of all currently available Party Quizzes. Did you know that you can also&nbsp;
            <Link to="/quizzes/create" title="Create your own party quiz">
              create your own quiz using our quiz maker
            </Link>
            ?
          </p>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <div className="columns is-desktop">
          <div className="column">
            <DataFetcher url="/api/quizzes?limit=50&order=top">
              <QuizList/>
            </DataFetcher>
          </div>
        </div>
      </div>
    </section>
  </>);
};
