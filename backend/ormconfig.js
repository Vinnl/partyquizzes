const config = {
    "type": "postgres",
    // In development, entities and migrations are located in src/entity and src/migration, respectively.
    // However, after build+deployment the project is moved into the /backend directory.
    "entities": ["src/entity/*", "backend/src/entity/*"],
    "migrationsTableName": "migrations",
    "migrations": ["src/migration/*", "backend/src/migration/*"],
    "cli": {
        "migrationsDir": "src/migration"
    }
}

if (process.env.DATABASE_URL) {
    config.url = process.env.DATABASE_URL;
} else {
    config.host = "localhost";
    config.port = 5432;
    config.username = "postgres";
    config.password = "";
    config.database = "partyquizzes_db";
}

module.exports = config;
