import * as LocalForage from 'localforage';
import * as React from 'react';

import { getInjectedData } from '../getInjectedData';
import { Referrers } from '../lib/endpoints/injectedData';
import { ReferredByTwitter, RefferredByFacebook, RefferredByReddit } from '../lib/localForageInterfaces';
import { FacebookSharer } from './FacebookSharer';
import { RedditSharer } from './RedditSharer';
import { TwitterSharer } from './TwitterSharer';

export interface SharerProps {
  message: string;
  url: string;
  tags?: string[];
}

interface State {
  readonly target?: Referrers;
}

export class Sharer extends React.Component<SharerProps, State> {
  public readonly state: State = {};

  public async componentDidMount() {
    const injectedData = getInjectedData();

    // If the current page was arrived at through a specific network,
    // that network should always be the one to share with:
    if (injectedData.referrer) {
      this.setState({
        target: injectedData.referrer,
      });
    } else {
      const [ twitter, facebook, reddit ] = await Promise.all([
        LocalForage.getItem<ReferredByTwitter>('referredByTwitter'),
        LocalForage.getItem<RefferredByFacebook>('referredByFacebook'),
        LocalForage.getItem<RefferredByReddit>('referredByReddit'),
      ]);

      if (facebook) {
        this.setState({ target: 'facebook' });
      } else if(twitter) {
        this.setState({ target: 'twitter' });
      } else if(reddit) {
        this.setState({ target: 'reddit' });
      } else {
        this.setState({ target: 'facebook' });
      }
    }
  }

  public render() {
    if (typeof this.state.target === 'undefined') {
      // Don't render the sharing form when we do not know the best network yet
      return null;
    }

    switch(this.state.target) {
      case 'facebook':
        return <FacebookSharer {...this.props}/>;
      case 'reddit':
        return <RedditSharer {...this.props}/>;
      case 'twitter':
        return <TwitterSharer {...this.props}/>;
    }
  }
}
