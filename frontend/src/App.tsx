import * as LocalForage from 'localforage';
import * as React from 'react';
import { BrowserRouter, Link, NavLink, NavLinkProps, Route, RouteComponentProps, withRouter } from 'react-router-dom';

import './App.css';
import logo from './logo.svg';

import { Faq } from './content/FAQ';
import { TipsTricks } from './content/TipsTricks';
import { getInjectedData } from './getInjectedData';
import { LandingPage } from './LandingPage';
import { ReferredByTwitter, RefferredByFacebook, RefferredByReddit } from './lib/localForageInterfaces';
import { Quiz } from './quiz/Quiz';
import { QuizArchivePage } from './QuizArchivePage';
import { QuizCreator } from './quizCreator/QuizCreator';
import { RandomQuiz } from './RandomQuiz';
import { ReactGAListener } from './ReactGaListener';

// tslint:disable-next-line:no-empty-interface
interface Props {}

const injectedData = getInjectedData();
if (injectedData.referrer && injectedData.referrer === 'twitter') {
  LocalForage.setItem<ReferredByTwitter>('referredByTwitter', true);
}
if (injectedData.referrer && injectedData.referrer === 'facebook') {
  LocalForage.setItem<RefferredByFacebook>('referredByFacebook', true);
}
if (injectedData.referrer && injectedData.referrer === 'reddit') {
  LocalForage.setItem<RefferredByReddit>('referredByReddit', true);
}

const App: React.SFC<Props> = () => (
  <BrowserRouter>
    <ReactGAListener>
      <header className="section is-hidden-touch">
        <nav className="navbar" role="navigation" aria-label="main navigation">
          <div className="container">
            <div className="navbar-brand">
              {/*
                The maxWidth is necessary not to overflow the screen on mobile in Firefox
                (Developer Edition, at the time of writing). Removing the `navbar-item`
                does the trick as well, but since Bulma's docs do mention using navbar-items,
                it's probably best to keep using it.
                */}
              <Link to="/" title="View the homepage" className="navbar-item" style={{ maxWidth: '100%' }}>
                <img
                  src={logo}
                  alt={process.env.REACT_APP_NAME}
                  style={{ maxHeight: '100px' }}
                />
              </Link>
            </div>
            <div className="navbar-menu">
              <div className="navbar-end">
                <div className="navbar-item">
                  <NavLink to="/quizzes/create" className="button is-large" activeClassName="is-active">Quiz Maker</NavLink>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </header>
      <header className="is-hidden-desktop">
        <nav className="tabs is-centered is-boxed is-fullwidth is-medium">
          <ul>
            <NavTab to="/" className="has-text-weight-bold" activeClassName="is-active">Party Quizzes</NavTab>
            <NavTab to="/quizzes/create" activeClassName="is-active">Quiz Maker</NavTab>
          </ul>
        </nav>
      </header>
      <Route path="/" component={LandingPage} exact={true}/>
      <Route path="/random" component={RandomQuiz} exact={true}/>
      <Route path="/faq" component={Faq} exact={true}/>
      <Route path="/tips-tricks" component={TipsTricks} exact={true}/>
      <Route path="/quiz/:id" component={Quiz} exact={true}/>
      <Route path="/quizzes" component={QuizArchivePage} exact={true}/>
      <Route path="/quizzes/create" component={QuizCreator} exact={true}/>
      <footer className="footer">
        <div className="container">
          <div className="columns">
            <div className="column is-one-third-desktop">
              <Link
                to="/"
                title="Home"
                className="is-size-4 button is-large is-text"
              >Home</Link>
              <hr/>
              <ul>
                <li>
                  <Link
                    to="/quizzes"
                    className="is-size-5 button is-text"
                    title="View all Party Quizzes"
                  >All Party Quizzes</Link>
                </li>
                <li>
                  <Link
                    to="/quizzes/create"
                    className="is-size-5 button is-text"
                    title="Make your own Party Quiz"
                  >Make your own Party Quiz</Link>
                </li>
              </ul>
            </div>
            <div className="column is-offset-one-third-desktop">
              <ul>
                <li>
                  <Link
                    to="/faq"
                    title="Frequently Asked Questions"
                  >Frequently Asked Questions</Link>
                </li>
                <li><a href="mailto:contact@partyquizzes.com" title="Send us an email">Contact us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </ReactGAListener>
  </BrowserRouter>
);

export default App;

const BareNavTab: React.SFC<RouteComponentProps<{}> & NavLinkProps> = (props) => {
  const className = (props.location.pathname === props.to)
    ? props.activeClassName
    : undefined;

  return (
    <li className={className}>
      <NavLink to={props.to} className={props.className} activeClassName={props.activeClassName}/>
    </li>
  );
 };
const NavTab = withRouter(BareNavTab);