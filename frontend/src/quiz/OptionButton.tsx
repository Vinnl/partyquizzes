import * as React from "react";

interface Props {
  quote: string;
  optionIndex: number;
  onAnswer: (selectedOption: number) => void;
}

export const OptionButton: React.SFC<Props> = props => {
  return (
    <button
      itemProp="suggestedAnswer"
      itemScope={true}
      itemType="https://schema.org/Answer"
      onClick={getHandler(props.optionIndex, props.onAnswer)}
      className="button is-large"
    >
      <span itemProp="text">{props.quote}</span>
    </button>
  );
};

function getHandler(index: number, callback: (selectedOption: number) => void) {
  return (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();

    callback(index);
  }
}
