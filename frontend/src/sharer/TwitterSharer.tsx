import * as React from 'react';
import { OutboundLink } from 'react-ga';

import './Sharer.css';

import { SharerProps } from './Sharer';

export const TwitterSharer: React.SFC<SharerProps> = (props) => { 
  const link = encodeURIComponent(props.url);
  // We add two newlines, because Twitter sticks the link right after the tweet,
  // which makes the tweet itself somewhat unreadable.
  // Especially since they're displaying a card for the link,
  // adding some whitespace above it is fine.
  const tweet = encodeURIComponent(props.message + '\n\n');
  const hashtags = encodeURIComponent((props.tags || []).map(tag => tag.replace(' ', '')).join(','));

  return (
    <aside className="box sharer">
      <p className="content">
        {props.message} {formatTags(props.tags)}
      </p>
      <div className="field">
        <div className="control has-text-right">
        <OutboundLink
          to={`https://twitter.com/intent/tweet?url=${link}&text=${tweet}&hashtags=${hashtags}`}
          title="Tweet this"
          className="button twitter"
          target="_blank"
          eventLabel="share-twitter"
        >
          Tweet
        </OutboundLink>
        </div>
      </div>
    </aside>
  );
 };

function formatTags(tags?: string[]) {
  if (!tags) {
    return '';
  }

  return tags.map(tag => '#' + tag.replace(' ', '')).join(' ');
}
