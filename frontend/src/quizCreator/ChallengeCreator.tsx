import * as React from 'react';

import './ChallengeCreator.css';

// tslint:disable-next-line:no-empty-interface
interface Props {
  id: number;
  onComplete: (challengeIndex: number, challenge: Challenge) => void;
  challenge?: Challenge;
  suggestedChallenge?: Challenge;
}

interface State {
  readonly form: {
    readonly title: string;
    readonly solution: string;
    readonly otherOption: string;
  };

  readonly editing: boolean;
}

export interface Challenge {
  title: string;
  solution: string;
  otherOptions: [ string ];
}

export class ChallengeCreator extends React.Component<Props, State> {
  public readonly state: State = {
    editing: false,
    form: this.props.challenge
      ? {
        otherOption: this.props.challenge.otherOptions[0],
        solution: this.props.challenge.solution,
        title: this.props.challenge.title,
      }
      : this.props.suggestedChallenge
        ? {
          otherOption: this.props.suggestedChallenge.otherOptions[0],
          solution: this.props.suggestedChallenge.solution,
          title: '',
        }
        : { otherOption: '', solution: '', title: '' },
  };

  public constructor(props: Props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.startEditing = this.startEditing.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.onBlurForm = this.onBlurForm.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeSolution = this.onChangeSolution.bind(this);
    this.onChangeIncorrectOption = this.onChangeIncorrectOption.bind(this);
  }

  public render() {
    if (this.props.challenge && this.state.editing === false) {
      return this.renderShield(this.props.challenge);
    }

    return this.renderForm();
  }

  private renderShield(challenge: Challenge) {
    return (
      <div
        onClick={this.startEditing}
        onKeyPress={this.onKeyPress}
        tabIndex={0}
        role="button"
        aria-label="Edit this challenge"
        className="card challenge"
      >
        <div className="card-content">
          <div className="title is-5">
            {challenge.title}
          </div>
          <table className="table">
            <tbody>
              <tr>
                <th className="is-narrow">
                  <span className="tag is-success">Correct</span>&nbsp;
                </th>
                <td>
                  {challenge.solution}
                </td>
              </tr>
              <tr>
                <th className="is-narrow">
                  <span className="tag is-warning">Incorrect</span>&nbsp;
                </th>
                <td>
                  {challenge.otherOptions[0]}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  private renderForm() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="columns">
          <div className="column">
            <div className="field">
              <label
                className="label"
                htmlFor={`challengeTitle${this.props.id}`}
              >
                Challenge
              </label>
              <div className="control">
                <input
                  id={`challengeTitle${this.props.id}`}
                  data-challenge-id={this.props.id}
                  onChange={this.onChangeTitle}
                  onBlur={this.onBlurForm}
                  type="text"
                  autoFocus={this.state.editing || this.props.id > 0}
                  required={true}
                  className="input"
                  value={this.state.form.title}
                  placeholder={this.props.suggestedChallenge ? this.props.suggestedChallenge.title : '"White-collar? Blue-collar? I am collar-blind."'}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="field">
              <label
                htmlFor={`challenge${this.props.id}Solution`}
                className="label"
              >
                Correct answer
              </label>
              <div className="control">
                <input
                  id={`challenge${this.props.id}Solution`}
                  data-challenge-id={this.props.id}
                  onChange={this.onChangeSolution}
                  onBlur={this.onBlurForm}
                  type="text"
                  required={true}
                  className="input"
                  value={this.state.form.solution}
                  placeholder={this.props.suggestedChallenge ? this.props.suggestedChallenge.solution : 'Michael Scott'}
                />
              </div>
            </div>
            <div className="field">
              <label
                htmlFor={`challenge${this.props.id}IncorrectOption`}
                className="label"
              >
                Incorrect option
              </label>
              <div className="control">
                <input
                  id={`challenge${this.props.id}IncorrectOption`}
                  data-challenge-id={this.props.id}
                  onChange={this.onChangeIncorrectOption}
                  onBlur={this.onBlurForm}
                  type="text"
                  required={true}
                  className="input"
                  value={this.state.form.otherOption}
                  placeholder={this.props.suggestedChallenge ? this.props.suggestedChallenge.otherOptions[0] : 'Donald Trump'}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="buttons is-right">
          <button
            type="submit"
            data-challenge-id={this.props.id}
            onBlur={this.onBlurForm}
            disabled={!isValidForm(this.state)}
            className="button is-primary"
          >
            Add challenge
          </button>
        </div>
      </form>
    );
  }

  private startEditing() {
    this.setState({ editing: true });
  }

  private stopEditing() {
    this.setState({ editing: false });
  }

  private onKeyPress(event: React.KeyboardEvent<HTMLDivElement>) {
    // tslint:disable-next-line:no-console
    if(event.key === 'Enter' || event.key === ' ') {
      this.startEditing();
    }
  }

  // If the user blurs a form field and does *not* focus on a different form field,
  // exit edit mode and show the shield again.
  // (In-progress edits are maintained in the element's state.)
  private onBlurForm(event: React.FocusEvent<HTMLInputElement | HTMLButtonElement>) {
    if (
      event.relatedTarget !== null
      && typeof event.relatedTarget === 'object'
      && (event.relatedTarget as any).getAttribute
      && (event.relatedTarget as any).getAttribute('data-challenge-id') === this.props.id.toString()
    ) {
      return;
    }

    if (this.state.editing === true && this.props.challenge) {
      this.setState({ editing: false });
    }
  }

  private onChangeTitle(event: React.ChangeEvent<HTMLInputElement>) {
    const newValue = event.target.value;
    this.setState(prevState => ({ form: { ...prevState.form, title: newValue } }));
  }

  private onChangeSolution(event: React.ChangeEvent<HTMLInputElement>) {
    const newValue = event.target.value;
    this.setState(prevState => ({ form: { ...prevState.form, solution: newValue } }));
  }

  private onChangeIncorrectOption(event: React.ChangeEvent<HTMLInputElement>) {
    const newValue = event.target.value;
    this.setState(prevState => ({ form: { ...prevState.form, otherOption: newValue } }));
  }

  private onSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    if (!isValidForm(this.state)) {
      return;
    }

    this.stopEditing();
    this.props.onComplete(this.props.id, {
      otherOptions: [ this.state.form.otherOption ],
      solution: this.state.form.solution,
      title: this.state.form.title,
    });
  }
}

const isValidForm = (state: State) => {
  return state.form.title && state.form.solution && state.form.otherOption;
}