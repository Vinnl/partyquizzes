import * as LocalForage from 'localforage';
import * as React from 'react';

import { getInjectedData } from './getInjectedData';

export interface DataFetcherProps<ApiResponse> {
  data?: ApiResponse | null;
  dataBySource?: {
    injected?: ApiResponse;
    fetched?: ApiResponse | null;
  };
}

interface Props<ApiResponse> {
  children: React.ReactElement<DataFetcherProps<ApiResponse>>;
  url: string;
  processData?: (response: any) => ApiResponse;
}

interface State<ApiResponse> {
  readonly injected?: ApiResponse;
  readonly data?: null | ApiResponse;
}

const injectedData = getInjectedData();

export class DataFetcher<ApiResponse> extends React.Component<Props<ApiResponse>, State<ApiResponse>> {
  public readonly state: State<ApiResponse> = {};

  public componentDidMount() {
    this.fetchData();
  }

  public componentDidUpdate(previousProps: Props<ApiResponse>) {
    if (previousProps.url !== this.props.url) {
      this.fetchData();
    }
  }

  public render() {
    const props: DataFetcherProps<ApiResponse> & React.Attributes = {
      // Note: we're adding this.state.data twice, but intentionally :)
      // If it's null or undefined, we want to fall back to other data sources,
      // but if those are not available either and data fetching failed as well,
      // we want to return the result of the data fetching:
      data: this.state.data || this.state.injected || this.state.data,
      dataBySource: {
        fetched: this.state.data,
        injected: this.state.injected,
      },
      key: this.props.url,
    };

    return React.cloneElement(this.props.children, props);
  }

  private async fetchData() {
    if (injectedData[this.props.url]) {
      this.setState({
        injected: this.processData(injectedData[this.props.url]),
      });
    } else {
      const cachedData = await LocalForage.getItem<ApiResponse>(this.props.url);

      if (cachedData) {
        this.setState({
          data: this.processData(cachedData),
        });
      }
      try {
        const response = await fetch(this.props.url);
        const data: ApiResponse = await response.json();

        if (!cachedData) {
          // Only pass the fetched data on to the child component if no cached data has been
          // provided yet, to prevent suddenly changing content.
          this.setState({
            data: this.processData(data),
          });
        }

        LocalForage.setItem<ApiResponse>(this.props.url, data);
      } catch (e) {
        if (!cachedData) {
          this.setState({ data: null });
        }
      }
    }
  }

  private processData(response: any): ApiResponse {
    if (this.props.processData) {
      return this.props.processData(response);
    }

    return response as ApiResponse;
  }
}
