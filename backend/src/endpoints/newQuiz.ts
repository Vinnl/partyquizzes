import { RequestHandler } from 'express';
import { getConnection, getRepository } from 'typeorm';

import { isValidQuiz, PostQuizRequest, PostQuizResponse } from '../../../frontend/src/lib/endpoints/quiz';
import { Challenge } from '../entity/challenge';
import { Option } from '../entity/option';
import { Quiz } from '../entity/quiz';

export const newQuizHandler: RequestHandler = async (req, res) => {
  const proposedQuiz: Partial<PostQuizRequest> = req.body;
  if (!isValidQuiz(proposedQuiz)) {
    res.status(400).send('Quiz is invalid or incomplete');

    return;
  }

  const connection = getConnection();
  const quizRepository = getRepository(Quiz);
  const challengeRepository = getRepository(Challenge);
  const optionRepository = getRepository(Option);

  const createdQuiz = await connection.transaction(async (transactionEntityManager) => {
    const quiz = quizRepository.create({
      description: proposedQuiz.description,
      title: proposedQuiz.title,
    });

    const challenges = await Promise.all(
      proposedQuiz.challenges.map(async (proposedChallenge) => {
        const options = proposedChallenge.options.map((proposedOption) => optionRepository.create({
          title: proposedOption.title,
        }))

        await transactionEntityManager.save(options);

        const challenge = challengeRepository.create({
          options,
          solution: options[proposedChallenge.solution],
          title: proposedChallenge.challenge,
        });

        return await transactionEntityManager.save(challenge);
      }
    ));

    quiz.challenges = challenges;

    return await transactionEntityManager.save(quiz);
  });

  const response: PostQuizResponse = {
    id: createdQuiz.id,
  };

  res.json(response);

  return;
};
