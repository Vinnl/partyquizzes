import * as LocalForage from 'localforage';
import * as React from 'react';
import { Link } from 'react-router-dom';

import { DataFetcherProps } from './DataFetcher';
import { ErrorPage } from './ErrorPage';
import { GetQuizzesResponse } from './lib/endpoints/quiz';
import { QuizScores } from './lib/localForageInterfaces';
import { LoadingPage } from './LoadingPage';

type Props = DataFetcherProps<GetQuizzesResponse> & {
  size?: 'medium' | 'small'
};

interface State {
  readonly quizScores?: QuizScores;
}

export class QuizList extends React.Component<Props, State> {
  public state: State = {};
  public props: Props = { size: 'medium' };

  public async componentDidMount() {
    const quizScores = await LocalForage.getItem<QuizScores>('quizScores');

    if (quizScores) {
      this.setState({ quizScores });
    }
  }

  public render() {
    if (typeof this.props.data === 'undefined') {
      return (
        <LoadingPage>
          Loading quizzes&hellip;
        </LoadingPage>
      );
    }
    if (this.props.data === null) {
      return (
        <ErrorPage>
          There was a problem loading quizzes, please try again.
        </ErrorPage>
      );
    }

    const list = this.props.data.map((quiz, index) => (
      <li key={`quiz${index}`} className="media">
        <div itemScope={true} itemType="https://schema.org/Game" className="media-content">
          <Link
            to={`/quiz/${quiz.id}`}
            itemProp="url"
            title={`Take the quiz: ${quiz.title}`}
            className={(this.props.size === 'small' ? 'is-size-5' : 'is-size-3 is-size-4-touch')}
          >
            <span itemProp="name">{quiz.title}</span>
          </Link>
          { quiz.description ? <p itemProp="description" className="content">{quiz.description}</p> : null }
          {this.renderScore(quiz.id)}
        </div>
      </li>
    ));
  
    return (
      <ul>
        {list}
      </ul>
    );
  }

  private renderScore(quizId: number) {
    if (!this.state.quizScores || !this.state.quizScores[quizId]) {
      return null;
    }
    const score = this.state.quizScores[quizId].score;
    const total = this.state.quizScores[quizId].total;

    return (
      <span className="tags has-addons">
        <span className="tag">
          Score
        </span>
        <span className={(score === total) ? 'tag is-success' : 'tag is-info'}>
          {score}/{total}
        </span>
      </span>
    );
  }
};
